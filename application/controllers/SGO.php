<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SGO extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('SGO_model');
//        $this->template->add_scss('scss/_firma.scss', 1);
        $this->template->add_css('vendor/datatables/dataTables.bootstrap4.min.css');
        $this->template->add_css('vendor/timepicker/jquery.timepicker.min.css');
        $this->template->add_js('vendor/datatables/jquery.dataTables.min.js');
        $this->template->add_js('vendor/datatables/dataTables.bootstrap4.min.js');
        $this->template->add_js('js/liberacion_vuelo_despacho.js', 1);
        $this->template->add_js('vendor/timepicker/jquery.timepicker.min.js');

//        $this->template->add_js('js/firma.js', 1);
        is_logged_in();
    }

    public function index() {
        $this->template->load('sgo/v_inicio');
    }

    public function pesosCombustibles() {
        $this->template->load('sgo/v_pesos_combustibles');
    }

    public function liberacionVueloDespacho() {
        $this->template->load('sgo/v_liberacion_vuelo_despacho');
    }

    public function weightAndBalanceLoadSheet() {
        $this->template->load('sgo/v_weight_and_balance_load_sheet');
    }

    public function planVuelo() {
        $this->template->load('sgo/v_plan_vuelo');
    }

    public function planVueloNavegado() {
        $this->template->add_js('js/sgo/navegado.js', 1);
        $data['res'] = $this->SGO_model->ShowNavegable();
        $permisos = $this->SGO_model->obtenerPermisosUsuario(auth()->idUsuario);
        foreach ($permisos as $permiso) {
            $array[] = $permiso->descripcion;
        }
        $data['permisos'] = $array;
        $this->template->load('sgo/navegado/v_plan_vuelo_navegado', $data);
    }

    public function Enviar() {
        $xss_post = $this->input->post(NULL, TRUE);
//            var_dump($xss_post);die;
        $dataNavegable['user_id'] = auth()->idUsuario; //defecto
        $dataNavegable['titulo'] = $xss_post['titulo'];
        $dataNavegable['subtitulo'] = $xss_post['subtitulo'];
        $dataNavegable['textofecha'] = $xss_post['textofecha'];
        $dataNavegable['ete'] = $xss_post['ete'];
        $dataNavegable['distance'] = $xss_post['distance'];
        $dataNavegable['avgwind'] = $xss_post['avgwind'];
        $dataNavegable['etd'] = $xss_post['etd'];
        $dataNavegable['eta'] = $xss_post['eta'];
        $dataNavegable['avgtas'] = $xss_post['avgtas'];
        $dataNavegable['altitude'] = $xss_post['altitude'];
        $dataNavegable['route'] = $xss_post['route'];
        $dataNavegable['departureatis'] = $xss_post['departureatis'];
        $dataNavegable['clearance'] = $xss_post['clearance'];
        $dataNavegable['arrivalatis'] = $xss_post['arrivalatis'];
        $dataNavegable['ramp'] = $xss_post['ramp'];

        $dataNavegable['flightregion'] = "";
        $dataNavegable['eet'] = "";
        $dataNavegable['entry'] = "";
        $dataNavegable['exit'] = "";
        $dataNavegable['distanceb'] = "";
        $dataNavegable['ip'] = $this->input->ip_address();
        $navegable_id = $this->SGO_model->InsertarNavegable($dataNavegable);


        foreach ($xss_post['flight_1'] as $key => $flight) {
            $dataFlight['tnavegable_id'] = $navegable_id;
            $dataFlight['flightregion'] = $flight['flightregion'];
            $dataFlight['eet'] = $flight['eet'];
            $dataFlight['entry'] = $flight['entry'];
            $dataFlight['exit'] = $flight['exit'];
            $dataFlight['distanceb'] = $flight['distanceb'];

            $this->SGO_model->InsertartFlight('tflight1', $dataFlight);
        }
        if (isset($xss_post['flight_2'])) {
            foreach ($xss_post['flight_2'] as $key => $flight) {
                $dataFlight['tnavegable_id'] = $navegable_id;
                $dataFlight['flightregion'] = $flight['flightregion'];
                $dataFlight['eet'] = $flight['eet'];
                $dataFlight['entry'] = $flight['entry'];
                $dataFlight['exit'] = $flight['exit'];
                $dataFlight['distanceb'] = $flight['distanceb'];

                $this->SGO_model->InsertartFlight('tflight2', $dataFlight);
            }
        }
        foreach ($xss_post['fuel'] as $key => $fuel) {
            $dataFuel['tnavegable_id'] = $navegable_id;
            $dataFuel['fuel'] = $fuel['fuel'];
            $dataFuel['lbs'] = $fuel['lbs'];
            $dataFuel['time'] = $fuel['time'];
            $this->SGO_model->InsertarFuel($dataFuel);
        }
        foreach ($xss_post['weight'] as $key => $weight) {
            $dataWeight['tnavegable_id'] = $navegable_id;
            $dataWeight['weight'] = $weight['weight'];
            $dataWeight['numero'] = $weight['numero'];
            $this->SGO_model->InsertarWeight($dataWeight);
        }
        foreach ($xss_post['info'] as $key => $info) {
            $dataInfo['tnavegable_id'] = $navegable_id;
            $dataInfo['info'] = $info['info'];
            $dataInfo['item'] = $info['item'];
            $this->SGO_model->InsertarInfo($dataInfo);
        }
        foreach ($xss_post['notes'] as $key => $note) {
            $dataNote['tnavegable_id'] = $navegable_id;
            $dataNote['fila1'] = $note['fila1'];
            $dataNote['fila2'] = $note['fila2'];
            $dataNote['fila3'] = $note['fila3'];
            $this->SGO_model->InsertarNote($dataNote);
        }
        foreach ($xss_post['rutas'] as $key => $ruta) {
            $dataRuta = [];
            $dataRuta['tnavegable_id'] = $navegable_id;
            $dataRuta['fila1'] = $ruta['fila1'];
            $dataRuta['columna'] = $ruta['columna'];
            if ($ruta['columna'] == 1) {
                $dataRuta['fila2'] = $ruta['fila2'];
                $dataRuta['fila3'] = $ruta['fila3'];
                $dataRuta['fila4'] = $ruta['fila4'];
                $dataRuta['fila5'] = $ruta['fila5'];
                $dataRuta['fila6'] = $ruta['fila6'];
                $dataRuta['fila7'] = $ruta['fila7'];
                $dataRuta['fila8'] = $ruta['fila8'];
                $dataRuta['fila9'] = $ruta['fila9'];
                $dataRuta['fila10'] = $ruta['fila10'];
                $dataRuta['fila11'] = $ruta['fila11'];
            }
            $this->SGO_model->InsertarRuta($dataRuta);
        }
        foreach ($xss_post['latitudes'] as $key => $latitude) {
            $datLatitude['tnavegable_id'] = $navegable_id;
            $datLatitude['fila1'] = $latitude['fila1'];
            $datLatitude['fila2'] = $latitude['fila2'];
            $datLatitude['fila3'] = $latitude['fila3'];
            $datLatitude['fila4'] = $latitude['fila4'];
            $datLatitude['fila5'] = $latitude['fila5'];
            $datLatitude['fila6'] = $latitude['fila6'];
            $datLatitude['fila7'] = $latitude['fila7'];
            $datLatitude['fila8'] = $latitude['fila8'];
            $this->SGO_model->InsertarLatitude($datLatitude);
        }
        foreach ($xss_post['airports'] as $key => $airport) {
            $dataAirport['tnavegable_id'] = $navegable_id;
            $dataAirport['fila1'] = $airport['fila1'];
            $dataAirport['fila2'] = $airport['fila2'];
            $dataAirport['fila3'] = $airport['fila3'];
            $dataAirport['fila4'] = $airport['fila4'];
            $dataAirport['fila5'] = $airport['fila5'];
            $dataAirport['fila6'] = $airport['fila6'];
            $dataAirport['fila7'] = $airport['fila7'];
            $dataAirport['fila8'] = $airport['fila8'];
            $dataAirport['fila9'] = $airport['fila9'];
            $dataAirport['fila10'] = $airport['fila10'];
            $this->SGO_model->InsertarAirport($dataAirport);
        }
        $res['res'] = $this->SGO_model->ShowNavegable();
        echo $this->load->view('sgo/navegado/v_tbody', $res, TRUE);
    }

    public function Ver($id) {
        $res = $this->SGO_model->VerNavegable($id);
        $res->fuel = $this->SGO_model->VerDetalleNavegable('tfuel', $id);
        $res->weight = $this->SGO_model->VerDetalleNavegable('tweight', $id);
        $res->info = $this->SGO_model->VerDetalleNavegable('tinfo', $id);
        $res->notes = $this->SGO_model->VerDetalleNavegable('tnotes', $id);
        $res->rutas = $this->SGO_model->VerDetalleNavegable('trutas', $id);
        $res->latitudes = $this->SGO_model->VerDetalleNavegable('tlatitudes', $id);
        $res->airports = $this->SGO_model->VerDetalleNavegable('tairports', $id);
        $res->flight_1 = $this->SGO_model->VerDetalleNavegable('tflight1', $id);
        $res->flight_2 = $this->SGO_model->VerDetalleNavegable('tflight2', $id);
        $data['res'] = $res;
        $this->load->view('sgo/navegado/v_ver', $data);
    }

    public function Editar($id) {
        $this->template->add_css('vendor/materialize/materialize.min.css');
        $this->template->add_css('css/sgo/editar.css', 1);
        $this->template->add_js('vendor/materialize/materialize.min.js');
        $this->template->add_js('js/inputmask/jquery.inputmask.js');
        $this->template->add_js('vendor/moment/moment.min.js');
        $this->template->add_js('js/sgo/editar.js', 1);

        // $fp = fsockopen("www.google.com", 80);
        // var_dump($fp);die;

        $res = $this->SGO_model->VerNavegable($id);
        $res->fuel = $this->SGO_model->VerDetalleNavegable('tfuel', $id);
        $res->weight = $this->SGO_model->VerDetalleNavegable('tweight', $id);
        $res->info = $this->SGO_model->VerDetalleNavegable('tinfo', $id);
        $res->notes = $this->SGO_model->VerDetalleNavegable('tnotes', $id);
        $res->rutas = $this->SGO_model->VerDetalleNavegable('trutas', $id);
        $res->latitudes = $this->SGO_model->VerDetalleNavegable('tlatitudes', $id);
        $res->airports = $this->SGO_model->VerDetalleNavegable('tairports', $id);
        $res->flight_1 = $this->SGO_model->VerDetalleNavegable('tflight1', $id);
        $res->flight_2 = $this->SGO_model->VerDetalleNavegable('tflight2', $id);
        // echo "<pre>";
        // var_dump($res);die;
        $data['res'] = $res;
        $this->template->load('sgo/navegado/v_editar', $data);
    }

    public function EditarTNavegable() {
        $div_valor = $this->input->get('div_valor');
        $div_id = $this->input->get('div_id');
        $nom_columna = $this->input->get('nom_columna');

        $datos = array('' . $nom_columna . '' => $div_valor);
        $where = array('id' => $div_id);
        $this->SGO_model->EditarTabla('tnavegable', $datos, $where);
    }

    public function EditarTFuel() {
        $fuel = $this->input->get('fuel');
        $tnavegable_id = $this->input->get('tnavegable_id');
        $actuals = $this->input->get('actuals');
        $datos = array('actuals' => $actuals, 'user_edit' => auth()->codigoUsuario);
        $where = array(
            'fuel' => $fuel,
            'tnavegable_id' => $tnavegable_id
        );
        $this->SGO_model->EditarTabla('tfuel', $datos, $where);
    }

    public function EditarTRutas() {
        $fila_id = $this->input->get('fila_id');
        $fila_valor = $this->input->get('fila_valor');
        $cadena = $this->input->get('tnavegable_id');
        $cadena_id = explode('-', $cadena);
        $tnavegable_id = trim($cadena_id[0]);
        $id = trim($cadena_id[1]);

        $fila1 = $this->input->get('fila1');
        $fila = $this->input->get('fila');

        $fila1_html = explode("<br>", $fila1);
        $fila1_html_1 = trim($fila1_html[0]);
        $fila1_html_2 = trim($fila1_html[1]);

        $datos = array('' . $fila . '' => $fila_valor, 'user_edit' => auth()->codigoUsuario);

        $where = array(
            'fila1' => '' . $fila1_html_1 . '
            <br>
            ' . $fila1_html_2 . '',
            'tnavegable_id' => $tnavegable_id,
            'id' => $id
        );
        $this->SGO_model->EditarTabla('trutas', $datos, $where);
    }

    public function EditarTablaRutas() {

        $fila = $this->input->get('fila');
        $fila_valor = $this->input->get('fila_valor');
        $tnavegable_id = $this->input->get('tnavegable_id');
        $id = $this->input->get('id_trutas');

        $datos = array('' . $fila . '' => $fila_valor, 'user_edit' => auth()->codigoUsuario);

        $where = array(
            'tnavegable_id' => $tnavegable_id,
            'id' => $id
        );
        $this->SGO_model->EditarTabla('trutas', $datos, $where);
    }

    public function EditarTNotes() {
        $tnavegable_id = $this->input->get('tnavegable_id');
        $fila2 = $this->input->get('fila2');
        $nombre_td = $this->input->get('nombre_td');
        $valor = $this->input->get('valor');
        echo $fila2;
        $datos = array('' . $nombre_td . '' => $valor, 'user_edit' => auth()->codigoUsuario);

        $where = array(
            'fila2' => $fila2,
            'tnavegable_id' => $tnavegable_id
        );
        var_dump($datos, $where);
        $this->SGO_model->EditarTabla('tnotes', $datos, $where);
    }

    public function listaFormularios() {
        $data['subelementos'] = $this->SGO_model->obtenerSubelementosSGO();
        $this->template->add_js('js/sgo/table.api.js');
        $this->template->add_js('js/sgo/lista_formularios.js', 1);
        $this->template->add_css('css/sgo/lista_formularios.css', 1);
        $this->template->load('sgo/v_lista_formularios', $data);
    }

    public function GenerarPDF($id) {
        $res = $this->SGO_model->VerNavegable($id);
        $res->fuel = $this->SGO_model->VerDetalleNavegable('tfuel', $id);
        $res->weight = $this->SGO_model->VerDetalleNavegable('tweight', $id);
        $res->info = $this->SGO_model->VerDetalleNavegable('tinfo', $id);
        $res->notes = $this->SGO_model->VerDetalleNavegable('tnotes', $id);
        $res->rutas = $this->SGO_model->VerDetalleNavegable('trutas', $id);
        $res->latitudes = $this->SGO_model->VerDetalleNavegable('tlatitudes', $id);
        $res->airports = $this->SGO_model->VerDetalleNavegable('tairports', $id);
        $res->flight_1 = $this->SGO_model->VerDetalleNavegable('tflight1', $id);
        $res->flight_2 = $this->SGO_model->VerDetalleNavegable('tflight2', $id);
        $data['res'] = $res;
        $this->load->view('sgo/navegado/generar_pdf', $data);

        $html = $this->output->get_output();
        $this->load->library('pdf');
        $this->dompdf->loadHtml($html);
        $this->dompdf->setPaper('A4', 'landscape');
        $this->dompdf->render();
        $this->dompdf->stream("form.pdf", array("Attachment" => 0));
    }

    public function EliminarRegistro() {
        $id = trim($this->input->get('id'));
        $this->SGO_model->Eliminar('tnavegable', $id);

        $this->SGO_model->Eliminar('tfuel', $id);
        $this->SGO_model->Eliminar('tweight', $id);
        $this->SGO_model->Eliminar('tinfo', $id);
        $this->SGO_model->Eliminar('tnotes', $id);
        $this->SGO_model->Eliminar('trutas', $id);
        $this->SGO_model->Eliminar('tlatitudes', $id);
        $this->SGO_model->Eliminar('tairports', $id);
        $this->SGO_model->Eliminar('tflight1', $id);
        $this->SGO_model->Eliminar('tflight2', $id);

        echo "ok";
    }

    public function AceptarRegistroTNavegable() {
        $id = $this->input->get('id');
        $datos = array('aceptar_registro' => 'S');
        $where = array('id' => $id);
        $this->SGO_model->aceptarRegistroTNavegable('tnavegable', $datos, $where);
    }

    public function BuscarFirma() {
        $rpta = $this->SGO_model->buscarFirma(auth()->idUsuario);
        echo $rpta;
    }

    public function validarCampos() {
        $id = $this->input->get('id');
        $res = $this->SGO_model->VerNavegable($id);
        $ramp = $res->ramp;
        $departureatis = $res->departureatis;
        $clearance = $res->clearance;
        if ($ramp !== '' && $departureatis !== '' && $clearance !== '') {
            $rpta = 'correcto';
        } else {
            if ($ramp == '') {
                $rpta = 'falta_ramp';
            } else if ($departureatis == '') {
                $rpta = 'falta_departureatis';
            } else {
                $rpta = 'falta_clearance';
            }
        }
        return $rpta;
    }

    public function CerrarRegistroTNavegable() {
        $id = $this->input->get('id');
        $rpta = $this->validarCampos($id);
        if ($rpta == 'correcto') {
            $datos1 = array('item' => auth()->firma);
            $where1 = array('info' => 'Signature', 'tnavegable_id' => $id);
            $firma = $this->SGO_model->firmarTNavegable('tinfo', $datos1, $where1);
            if ($firma) {
                $datos = array('cerrar_registro' => 'S');
                $where = array('id' => $id);
                $res = $this->SGO_model->cerrarRegistroTNavegable('tnavegable', $datos, $where);
                echo $res;
            }
        } else {
            echo $rpta;
        }
    }

}
