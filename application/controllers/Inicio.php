<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('upload');

        $this->load->model('Login_model');
//        $this->template->add_css('css/iniciar_sesion.css');
        $this->template->add_js('js/iniciar_sesion.js', 1);
//        $this->template->add_js('js/inicio/signature_pad.min.js', 1);
        $this->template->add_js('js/inicio/perfil.js', 1);
//        $this->template->add_css('css/inicio/firmar.css', 1);
        $this->template->add_js('js/inicio/signature_pad.umd.js', 1);
        $this->template->add_js('js/inicio/app.js', 1);

        $this->template->add_css('css/inicio/signature-pad.css', 1);
    }

    public function index() {
        $data = array();
        $data['error'] = $this->session->flashdata('error');
        $this->template->loadInicio('inicio/v_contenido', $data);
    }

    public function iniciar_sesion() {
        $this->form_validation->set_rules('usuario', 'Usuario', 'trim|required');
        $this->form_validation->set_rules('clave', 'Contraseña', 'required');
        if ($this->form_validation->run() == FALSE) {
            header('Location: ' . base_url('inicio'));
        } else {
            $usuario = $this->input->post('usuario');
            $user = $this->Login_model->ObtenerUsuario($usuario);
            if (!empty($user)) {
                $clave = $this->input->post('clave');
                $pass = $this->Login_model->ObtenerClave($usuario);
                $password = trim($pass->clave);
                if ($clave == $password) {
                    $datosUsuario = $this->Login_model->ObtenerDatos($usuario, $pass->clave);
                    if (!empty($datosUsuario)) {
                        setUserData($datosUsuario);
                        header('Location: ' . base_url('welcome'));
                    }
                } else {
                    $this->session->set_flashdata('error', 'El usuario o la contraseña son incorrectos.');
                    header('Location: ' . base_url('inicio'));
                }
            } else {
                $this->session->set_flashdata('error', 'El usuario o la contraseña son incorrectos.');
                header('Location: ' . base_url('inicio'));
            }
        }
    }

    public function VerPerfil() {
        $this->template->load('inicio/v_perfil');
    }

    public function EditarDatosUsuario() {
        $nombres = $this->input->get('nombres');
        $ape_paterno = $this->input->get('ape_paterno');
        $ape_materno = $this->input->get('ape_materno');
        $correo = $this->input->get('correo');
        $datos = array('nombres' => $nombres, 'ape_paterno' => $ape_paterno, 'ape_materno' => $ape_materno, 'correo' => $correo);
        $where = array('id_usuario' => auth()->idUsuario);
        $this->Login_model->EditarTablaUsuario('usuarios', $datos, $where);
        $this->Salir();
    }

    public function SubirFirmaUsuario() {
        if (($_FILES["file"]["type"] == "image/pjpeg") || ($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/gif")) {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], "img/firmas/" . $_FILES['file']['name'])) {
                $datos = array('firma' => "img/firmas/" . $_FILES['file']['name']);
                $where = array('id_usuario' => auth()->idUsuario);
                $this->Login_model->EditarTablaUsuario('usuarios', $datos, $where);
                echo "img/firmas/" . $_FILES['file']['name'];
            } else {
                echo 0;
            }
        } else {
            echo 0;
        }
    }

    public function Salir() {
        $this->session->sess_destroy();
        header('Location: ' . base_url('inicio'));
    }

}
