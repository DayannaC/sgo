<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permiso extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Permiso_model');
        $this->template->add_css('vendor/datatables/dataTables.bootstrap4.min.css');
        $this->template->add_css('css/permiso.css');
        $this->template->add_js('vendor/datatables/jquery.dataTables.min.js');
        $this->template->add_js('vendor/datatables/dataTables.bootstrap4.min.js');
        $this->template->add_js('js/permiso.js', 1);
        is_logged_in();
    }

    public function index() {
        $data['elementos'] = $this->Permiso_model->obtenerElementos();
        $data['usuarios'] = $this->Permiso_model->obtenerUsuarios();
        $data['codigoElementos'] = $this->Permiso_model->obtenerCodigosElementos();
        $this->template->load('permiso/v_usuario', $data);
    }

    public function obtenerSubelementos() {
        $idElemento = $this->input->get('idElemento');
        $usuario = $this->input->get('usuario');
        $rpta = $this->Permiso_model->obtenerSubelementos($idElemento);
        echo json_encode($rpta);
    }

    public function cambiarEstadoUsuario() {
        $codUsuario = $this->input->get('codUsuario');
        $idUsuario = $this->input->get('idUsuario');
        $estado = $this->input->get('estado');
        if ($estado == 'activar') {
            if ($idUsuario !== '') {
                $datos = array('estado' => 'A');
                $where = "id_usuario=$idUsuario";
                $rpta = $this->Permiso_model->cambiarEstadoUsuario($datos, $where);
            } else {
                $datos = array('codigo' => $codUsuario);
                $rpta = $this->Permiso_model->crearUsuario($datos);
            }
        } else {
            $datos = array('estado' => 'I');
            $where = "id_usuario=$idUsuario";
            $rpta = $this->Permiso_model->cambiarEstadoUsuario($datos, $where);
        }
        echo $rpta;
    }

    public function darPermisos() {
        $usuario = $this->input->get('usuario');
        $idElemento = $this->input->get('idElemento');
        $idSubelemento = $this->input->get('idSubelemento');
        $accion = $this->input->get('accion');
        $idUsuario = $this->Permiso_model->obtenerIdUsuario($usuario);
        $rpta = $this->Permiso_model->buscarUsuarioSE($idUsuario, $idElemento, $idSubelemento);
        $filas = count($rpta);
        if ($accion == 'dar_permisos') {
            if ($filas == 0) {
                $datos = array('id_usuario' => $idUsuario, 'id_elemento' => $idElemento, 'id_subelemento' => $idSubelemento);
                $rpta = $this->Permiso_model->insertarUsuarioSE($datos);
                echo $rpta;
            } else {
                $estado = $rpta->estado;
                if ($estado == 'A') {
                    $datos = array('estado' => 'I');
                    $where = "id_usuario=$idUsuario AND id_elemento=$idElemento AND id_subelemento=$idSubelemento";
                    $rpta = $this->Permiso_model->actualizarUsuarioSE($datos, $where);
                    echo $rpta;
                } else {
                    $datos = array('estado' => 'A');
                    $where = "id_usuario=$idUsuario AND id_elemento=$idElemento AND id_subelemento=$idSubelemento";
                    $rpta = $this->Permiso_model->actualizarUsuarioSE($datos, $where);
                    echo $rpta;
                }
            }
        } else {
            if ($filas !== 0) {
                $estado = $rpta->estadoSE;
                echo $estado;
            }
        }
    }

    public function actualizarPermisos() {
        $this->template->load_menu(auth()->idUsuario);
    }

    public function agregarUsuario() {
        $data['roles'] = $this->Permiso_model->mostrarRoles();
        $this->template->load('permiso/v_adm_agregar_usuario', $data);
    }

    public function guardarUsuario() {
        $this->form_validation->set_rules('nombres', 'Nombres', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('ape_paterno', 'Apellido Paterno', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('ape_materno', 'Apellido Materno', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('correo', 'Correo', 'trim|required|max_length[100]');
        $this->form_validation->set_rules('rol', 'Rol', 'trim|required|max_length[100]');

        if ($this->form_validation->run() == FALSE) {
            $this->template->load('permiso/v_adm_agregar_usuario');
        } else {
            $correo = strtoupper($this->input->post('correo'));
            $user = $this->Permiso_model->buscarUsuario($correo);
            if (empty($user)) {
                $nombres = strtoupper($this->input->post('nombres'));
                $ape_paterno = strtoupper($this->input->post('ape_paterno'));
                $ape_materno = strtoupper($this->input->post('ape_materno'));
                $explode_nombres = explode(" ", $nombres);
                $primer_nombre = $explode_nombres[0];
                $usuario = $primer_nombre . '_' . $ape_paterno;
                $correo = strtoupper($this->input->post('correo'));
                $clave = '123456';
                $datos = array('codigo' => $usuario, 'nombres' => $nombres, 'ape_paterno' => $ape_paterno, 'ape_materno' => $ape_materno, 'correo' => $correo, 'clave' => $clave, 'estado' => 'A');
                $id_usuario = $this->Permiso_model->agregarUsuario($datos);
                $id_rol = $this->input->post('rol');
                $this->AgregarRol($id_usuario, $id_rol);
                $this->template->load('permiso/v_lista_usuarios');
            } else {
                $this->template->load('permiso/v_adm_agregar_usuario');
            }
        }
    }

    public function listaUsuarios() {
        $this->template->add_js('js/permiso/lista_usuarios.js', 1);
        $data['datosUsuarios'] = $this->Permiso_model->obtenerDatosUsuarios();
        $data['roles'] = $this->Permiso_model->mostrarRoles();

        $this->template->load('permiso/v_lista_usuarios', $data);
    }

    public function EditarAdmUsuario() {
        $id = $this->input->get('id');
        $nuevo_codigo = $this->input->get('nuevo_codigo');
        $nombres = $this->input->get('nombres');
        $ape_paterno = $this->input->get('ape_paterno');
        $ape_materno = $this->input->get('ape_materno');
        $correo = $this->input->get('correo');
        $id_rol = $this->input->get('id_rol');
        var_dump($id_rol);

        $datos = array('nombres' => $nombres, 'ape_paterno' => $ape_paterno, 'ape_materno' => $ape_materno, 'correo' => $correo, 'codigo' => $nuevo_codigo);
        $where = array('id_usuario' => $id);
        $this->Permiso_model->editarAdmUsuario('usuarios', $datos, $where);
        $this->Permiso_model->eliminarRolUsuario($id);
        $this->AgregarRol($id, $id_rol);
    }

    public function AgregarRol($id_usuario, $id_rol) {
        $permisos = $this->Permiso_model->obtenerPermisos($id_rol);
        foreach ($permisos as $permiso) {
            $p = $permiso['permisos'];
        }
        $array = explode(",", $p);
        $cant_permisos = count($array);
        for ($i = 0; $i < $cant_permisos; $i++) {
            $id_permiso = $array[$i];
            $datos = array('id_usuario' => $id_usuario, 'id_rol' => $id_rol, 'id_permiso' => $id_permiso);
            $this->Permiso_model->agregarRol($datos);
        }
    }

}
