<?php

class SGO_model extends CI_Model {

    protected $db_sgo;

    public function __construct() {
        parent::__construct();
        $this->db_sgo = $this->load->database('default', TRUE);
    }

    public function obtenerSubelementosSGO() {
        // SELECT sub.nombre FROM subelementos AS sub , elementos_subelementos AS elesub WHERE sub.id_subelemento=elesub.id_subelemento AND elesub.id_elemento=2 AND elesub.id_subelemento NOT LIKE '7' 
        $this->db_sgo->select('sub.nombre');
        $this->db_sgo->from('subelementos AS sub , elementos_subelementos AS elesub');
        $this->db_sgo->where('sub.id_subelemento=elesub.id_subelemento');
        $this->db_sgo->where('elesub.id_elemento=2');
        $this->db_sgo->where('elesub.id_subelemento !=', 7);
        $this->db_sgo->order_by('sub.orden');
        return $this->db_sgo->get()->result();
    }

    public function ShowNavegable() {
        $this->db_sgo->select('*');
        $this->db_sgo->from('tnavegable');
        $this->db_sgo->where('estado', 1);
        $this->db_sgo->order_by('id', 'DESC');
        $res = $this->db_sgo->get()->result();
        return $res;
    }

    public function VerNavegable($id) {
        $this->db_sgo->select('*');
        $this->db_sgo->from('tnavegable');
        $this->db_sgo->where('id', $id);
        $res = $this->db_sgo->get()->row();
        return $res;
    }

    public function VerDetalleNavegable($tabla, $id) {
        $this->db_sgo->select('*');
        $this->db_sgo->from($tabla);
        $this->db_sgo->where('tnavegable_id', $id);
        $this->db_sgo->where('estado', 1);
        $res = $this->db_sgo->get()->result();
        // $res = $this->db_sgo->get_compiled_select();
        // var_dump($res);die;
        return $res;
    }

    public function InsertartFlight($tabla, $data) {
        $res_insert = $this->db_sgo->insert($tabla, $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarNavegable($data) {
        $res_insert = $this->db_sgo->insert('tnavegable', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarFuel($data) {
        $res_insert = $this->db_sgo->insert('tfuel', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarWeight($data) {
        $res_insert = $this->db_sgo->insert('tweight', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarInfo($data) {
        $res_insert = $this->db_sgo->insert('tinfo', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarNote($data) {
        $res_insert = $this->db_sgo->insert('tnotes', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarRuta($data) {
        $res_insert = $this->db_sgo->insert('trutas', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarLatitude($data) {
        $res_insert = $this->db_sgo->insert('tlatitudes', $data);
        return $this->db_sgo->insert_id();
    }

    public function InsertarAirport($data) {
        $res_insert = $this->db_sgo->insert('tairports', $data);
        return $this->db_sgo->insert_id();
    }

    public function EditarTabla($tabla, $datos, $where) {
        $rpta = $this->db_sgo->update($tabla, $datos, $where);
        return $rpta;
    }

    public function Eliminar($tabla, $where) {
        if ($tabla == 'tnavegable') {
            $rpta = $this->db_sgo->update($tabla, array('estado' => 0), array('id' => $where));
        } else {
            $rpta = $this->db_sgo->update($tabla, array('estado' => 0), array('tnavegable_id' => $where));
        }
        return $rpta;
    }

    public function obtenerPermisosUsuario($id_usuario) {
        $this->db_sgo->select('urp.id_usuario, urp.id_rol, urp.id_permiso, p.descripcion, p.icono');
        $this->db_sgo->from('usuarios_roles_permisos AS urp');
        $this->db_sgo->join('permisos as p', 'urp.id_permiso = p.id_permiso');
        $this->db_sgo->where('urp.id_usuario', $id_usuario);
//        $this->db_sgo->where('urp.estado', 'A');
        return $this->db_sgo->get()->result();
    }

    public function buscarFirma($id_usuario) {
//        var_dump($id_usuario);
        $this->db_sgo->select('firma');
        $this->db_sgo->from('usuarios');
        $this->db_sgo->where('id_usuario', $id_usuario);
        return $this->db_sgo->get()->row()->firma;
    }

    public function aceptarRegistroTNavegable($tabla, $datos, $where) {
        $rpta = $this->db_sgo->update($tabla, $datos, $where);
        return $rpta;
    }

    public function cerrarRegistroTNavegable($tabla, $datos, $where) {
        $rpta = $this->db_sgo->update($tabla, $datos, $where);
        return $rpta;
    }

    public function firmarTNavegable($tabla, $datos, $where) {
        $rpta = $this->db_sgo->update($tabla, $datos, $where);
        return $rpta;
    }

}
