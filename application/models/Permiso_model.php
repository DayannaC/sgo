<?php

class Permiso_model extends CI_Model {

    protected $db_sgo;

    public function __construct() {
        parent::__construct();
        $this->db_sgo = $this->load->database('default', TRUE);
    }

    public function obtenerUsuarios() {
        $sql = "SELECT u.id_usuario as idUsuario,u.codigo,u.estado as estadoUsuario,
                (SELECT GROUP_CONCAT(usesu.id_elemento)
                from usuarios as u 
                join usuarios_elementos_subelementos as usesu 
                on u.id_usuario=usesu.id_usuario 
                WHERE usesu.id_usuario=u.id_usuario AND usesu.estado='A')
                as elementos
                FROM usuarios AS u";

        $query = $this->db_sgo->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    public function obtenerCodigosElementos() {
        $this->db_sgo->select('codigo,nombre,id_elemento as idElemento');
        $this->db_sgo->from('elementos');
        return $this->db_sgo->get()->result();
    }

    public function obtenerElementos() {
        $this->db_sgo->select('e.nombre');
        $this->db_sgo->from('elementos as e');
        return $this->db_sgo->get()->result();
    }

    public function obtenerSubelementos($idElemento) {
        $this->db_sgo->select('es.id_elemento as idElemento, e.nombre as nomElemento,e.codigo as codElemento, s.nombre as nomSubelemento,es.id_subelemento as idSubelemento');
        $this->db_sgo->from('elementos_subelementos as es');
        $this->db_sgo->join('elementos as e', 'es.id_elemento = e.id_elemento');
        $this->db_sgo->join('subelementos as s', 'es.id_subelemento = s.id_subelemento');
        $this->db_sgo->where('e.id_elemento', $idElemento);
        return $this->db_sgo->get()->result();
    }

    public function obtenerIdUsuario($usuario) {
        $this->db_sgo->select('id_usuario as idUsuario');
        $this->db_sgo->from('usuarios');
        $this->db_sgo->where('codigo', $usuario);
        return $this->db_sgo->get()->row()->idUsuario;
    }

    public function cambiarEstadoUsuario($datos, $where) {
        $rpta = $this->db_sgo->update('usuarios', $datos, $where);
        return $rpta;
    }

    public function crearUsuario($datos) {
        $rpta = $this->db_sgo->insert('usuarios', $datos);
        return $rpta;
    }

    public function buscarUsuarioSE($idUsuario, $idElemento, $idSubelemento) {
        $this->db_sgo->select('*,es.estado as estadoSE');
        $this->db_sgo->from('usuarios as u');
        $this->db_sgo->join('usuarios_elementos_subelementos as es', 'u.id_usuario=es.id_usuario');
        $this->db_sgo->where('es.id_usuario', $idUsuario);
        $this->db_sgo->where('es.id_elemento', $idElemento);
        $this->db_sgo->where('es.id_subelemento', $idSubelemento);
        return $this->db_sgo->get()->row();
    }

    public function insertarUsuarioSE($datos) {
        $rpta = $this->db_sgo->insert('usuarios_elementos_subelementos', $datos);
        return $rpta;
    }

    public function actualizarUsuarioSE($datos, $where) {
        $rpta = $this->db_sgo->update('usuarios_elementos_subelementos', $datos, $where);
        return $rpta;
    }

    public function agregarUsuario($datos) {
        $this->db_sgo->insert('usuarios', $datos);
        $id = $this->db_sgo->insert_id();
        return $id;
    }

    public function agregarRol($datos) {
        $rpta = $this->db_sgo->insert('usuarios_roles_permisos', $datos);
        return $rpta;
    }

    public function eliminarRolUsuario($id) {
        $rpta = $this->db_sgo->delete('usuarios_roles_permisos', array('id_usuario' => $id));
        return $rpta;
    }

    public function buscarUsuario($usuario) {
        $this->db_sgo->select('*');
        $this->db_sgo->from('usuarios');
        $this->db_sgo->where('correo', $usuario);
        return $this->db_sgo->get()->row();
    }

//    public function obtenerDatosUsuarios() {
//        $this->db_sgo->select('u.id_usuario,u.codigo,u.nombres,u.ape_paterno,u.ape_materno,u.correo,u.firma,u.clave,u.estado, u_r_p.id_rol,r.rol');
//        $this->db_sgo->from('usuarios AS u');
//        $this->db_sgo->join('usuarios_roles_permisos as u_r_p', 'u.id_usuario = u_r_p.id_usuario', 'left');
//        $this->db_sgo->join('roles AS r', 'r.id_rol = u_r_p.id_rol', 'left');
//        $this->db_sgo->group_by('u.id_usuario');
//        return $this->db_sgo->get()->result();
//    }

    public function obtenerDatosUsuarios() {
        $this->db_sgo->select('u.id_usuario,u.codigo,u.nombres,u.ape_paterno,u.ape_materno,u.correo,u.firma,u.clave,u.estado, u_r_p.id_rol,r.rol');
        $this->db_sgo->from('usuarios AS u');
        $this->db_sgo->join('usuarios_roles_permisos as u_r_p', 'u.id_usuario = u_r_p.id_usuario', 'left');
        $this->db_sgo->join('roles AS r', 'r.id_rol = u_r_p.id_rol', 'left');
        $this->db_sgo->group_by('u.id_usuario, u.codigo, u.nombres, u.ape_paterno, u.ape_materno, u.correo, u.firma, u.clave, u.estado, u_r_p.id_rol, r.rol');
        return $this->db_sgo->get()->result();
    }

    public function mostrarRoles() {
        $this->db_sgo->select('*');
        $this->db_sgo->from('roles');
        return $this->db_sgo->get()->result();
    }

    public function editarAdmUsuario($tabla, $datos, $where) {
        $rpta = $this->db_sgo->update($tabla, $datos, $where);
        return $rpta;
    }

    public function editarRol($id, $id_rol, $id_permiso) {

        $rpta = $this->db_sgo->update('usuarios_roles_permisos', $datos, $where);
        return $rpta;
    }

    public function obtenerPermisos($id_rol) {
        $sql = "SELECT GROUP_CONCAT(r_p.id_permiso) AS permisos
                from roles_permisos as r_p
                WHERE r_p.id_rol='.$id_rol.'";

        $query = $this->db_sgo->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
    }

    public function agregarRolUsuario($datos) {
        $this->db_sgo->insert('usuarios_roles_permisos', $datos);
    }

}
