<?php

class Login_model extends CI_Model {

    protected $db_sgo;

    public function __construct() {
        parent::__construct();
        $this->db_sgo = $this->load->database('default', TRUE);
    }

    public function ObtenerUsuario($usuario) {
        $this->db_sgo->select('u.id_usuario as idUsuario,u.nombres, u.estado');
        $this->db_sgo->from('usuarios as u');
        $this->db_sgo->where('u.codigo', $usuario);
        $this->db_sgo->where('u.estado', 'A');
        return $this->db_sgo->get()->row();
    }

    public function ObtenerClave($usuario) {
        $this->db_sgo->select('clave');
        $this->db_sgo->from('usuarios');
        $this->db_sgo->where('codigo', $usuario);
        return $this->db_sgo->get()->row();
    }

//    public function ObtenerDatos($usuario, $clave) {
//        $this->db_sgo->select('u.id_usuario as idUsuario,concat(u.nombres," ",u.ape_paterno," ",u.ape_materno) AS nombre,u.nombres as nombres,u.ape_paterno as ape_paterno,u.ape_materno as ape_materno,u.correo, u.codigo as codigoUsuario, u.firma, u.estado as estadoUsuario, u_r_p.id_rol, r.rol');
//        $this->db_sgo->from('usuarios as u');
//        $this->db_sgo->join('usuarios_roles_permisos as u_r_p', 'u.id_usuario = u_r_p.id_usuario');
//        $this->db_sgo->join('roles as r', 'r.id_rol = u_r_p.id_rol');
//        $this->db_sgo->where('u.codigo', $usuario);
//        $this->db_sgo->where('u.clave', $clave);
//        $this->db_sgo->group_by('u.id_usuario');
//        return $this->db_sgo->get()->row();
//    }
    public function ObtenerDatos($usuario, $clave) {
        $this->db_sgo->select('u.id_usuario as idUsuario,u.ape_paterno AS nombre,u.nombres as nombres,u.ape_paterno as ape_paterno,u.ape_materno as ape_materno,u.correo, u.codigo as codigoUsuario, u.firma, u.estado as estadoUsuario, u_r_p.id_rol, r.rol');
        $this->db_sgo->from('usuarios as u');
        $this->db_sgo->join('usuarios_roles_permisos as u_r_p', 'u.id_usuario = u_r_p.id_usuario');
        $this->db_sgo->join('roles as r', 'r.id_rol = u_r_p.id_rol');
        $this->db_sgo->where('u.codigo', $usuario);
        $this->db_sgo->where('u.clave', $clave);
        $this->db_sgo->group_by('u.id_usuario,u.nombres , u.ape_paterno,u.ape_materno, u.correo, u.codigo, u.firma, u.estado, r.id_rol, r.rol');
        return $this->db_sgo->get()->row();
    }

    public function ObtenerElementos($idUsuario) {
        $this->db_sgo->select('usesu.id_elemento as idElemento,e.nombre');
        $this->db_sgo->from('usuarios_elementos_subelementos as usesu');
        $this->db_sgo->join('elementos as e', 'e.id_elemento=usesu.id_elemento');
        $this->db_sgo->where('usesu.id_usuario', $idUsuario);
        $this->db_sgo->where('usesu.estado', 'A');
        $this->db_sgo->group_by('usesu.id_elemento,e.nombre');
        return $this->db_sgo->get()->result();
    }

    public function ObtenerSubelementos($idUsuario) {
        $this->db_sgo->select('usesu.id_elemento as idElemento, usesu.id_subelemento as idSubelemento, s.nombre,s.url,s.icono');
        $this->db_sgo->from('usuarios_elementos_subelementos as usesu');
        $this->db_sgo->join('subelementos as s', 's.id_subelemento = usesu.id_subelemento');
        $this->db_sgo->where('usesu.id_usuario', $idUsuario);
        $this->db_sgo->where('usesu.estado', 'A');
        return $this->db_sgo->get()->result();
    }

    public function EditarTablaUsuario($tabla, $datos, $where) {
        $rpta = $this->db_sgo->update($tabla, $datos, $where);
        return $rpta;
    }

}
