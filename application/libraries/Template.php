<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Template {
    var $styles = "";
    var $scripts = "";
    var $scripts_a = "";
    var $template_data = array();
    var $template_layout = 'plantilla/layout';
    var $template_layout_main = 'inicio/layout';
    var $page = "";
    var $muestra_calendario = TRUE;
    var $muestra_carousel = TRUE;
    var $class_body = 'inicio reservas-on';
    var $url;

    function set($name, $value) {
        $this->template_data[$name] = $value;
    }

    function set_url_html($url) {
        $this->url = $url;
    }

    function add_css($pathfile, $band = null) {
        $this->styles .= link_tag($pathfile . ($band == 1 ? '?v=' . strtotime("now") : ''));
    }

    function carga_pagina($argumento) {
        $this->page = $argumento;
    }

    function muestra_calendario($argumento_bool) {
        $this->muestra_calendario = $argumento_bool;
    }

    function muestra_carousel($argumento_bool) {
        $this->muestra_carousel = $argumento_bool;
    }

    function setea_body($class_body) {
        $this->class_body = $class_body;
    }

    function add_js($pathfile, $band = null) {
        $this->scripts .= script_tag($pathfile . ($band == 1 ? '?v=' . strtotime("now") : ''));
    }

    function add_js_analitics($pathfile) {
        $this->scripts_a .= script_tag($pathfile);
    }

    function load_menu($idUsuario) {
        $this->CI = &get_instance();
        $model = &get_instance();
        $model->load->model('Login_model');
        $data['elementosUsuario'] = $model->Login_model->ObtenerElementos($idUsuario);
        $data['subelementosUsuario'] = $model->Login_model->ObtenerSubelementos($idUsuario);
        return $this->CI->load->view('plantilla/sidebar', $data);
    }

    function load($view = '', $view_data = array(), $return = FALSE) {
        $this->CI = &get_instance();
        $model = &get_instance();
        $this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
        $this->template_data['styles'] = $this->styles;
        $this->template_data['scripts'] = $this->scripts;
        $this->template_data['scripts_analitics'] = $this->scripts_a;
        return $this->CI->load->view($this->template_layout, $this->template_data, $return);
    }

    function loadInicio($view = '', $view_data = array(), $return = FALSE) {
        $this->CI = &get_instance();
        $model = &get_instance();
        $this->set('contents', $this->CI->load->view($view, $view_data, TRUE));
        $this->template_data['styles'] = $this->styles;
        $this->template_data['scripts'] = $this->scripts;
        $this->template_data['scripts_analitics'] = $this->scripts_a;
        return $this->CI->load->view($this->template_layout_main, $this->template_data, $return);
    }

    function load_header() {
        return $this->CI->load->view("plantilla/head");
    }

    function load_footer() {
        return $this->CI->load->view("plantilla/footer");
    }

}

?>