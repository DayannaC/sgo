<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= base_url('welcome') ?>">
        <div class="sidebar-brand-icon">
            <img id="imagenSidebar" src="<?= base_url() ?>img/logo">
        </div>
        <div class="sidebar-brand-text mx-3"></div>
    </a>
    <hr class="sidebar-divider my-0">
    <li class="nav-item active">
        <a class="nav-link" href="<?= base_url('welcome') ?>">
            <i class="fas fa-fw fa-plane-departure"></i>
            <span>SGO</span></a>
    </li>
    <div id="sidebar">
        <?php foreach ($elementosUsuario as $elementos) { ?>
            <hr class="sidebar-divider">
            <div class="sidebar-heading">
                <?= $elementos->nombre ?>
            </div>
            <?php
            foreach ($subelementosUsuario as $subelementos) {
                if ($elementos->idElemento == $subelementos->idElemento) {
                    ?>  
                    <li class="nav-item">
                        <a class="nav-link" href="<?= base_url($subelementos->url) ?>">
                            <i class="<?= $subelementos->icono ?>"></i>
                            <span><?= $subelementos->nombre ?></span>
                        </a>
                    </li>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>

    <!--    <hr class="sidebar-divider">
        <div class="sidebar-heading">
            REFUND
        </div>
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePage" aria-expanded="true"
               aria-controls="collapsePage">
                <i class="fas fa-fw fa-columns"></i>
                <span>????</span>
            </a>
            <div id="collapsePage" class="collapse" aria-labelledby="headingPage" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Example Pages</h6>
                    <a class="collapse-item" href="login.html">Login</a>
                    <a class="collapse-item" href="register.html">Register</a>
                    <a class="collapse-item" href="404.html">404 Page</a>
                    <a class="collapse-item" href="blank.html">Blank Page</a>
                </div>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="charts.html">
                <i class="fas fa-fw fa-chart-area"></i>
                <span>????</span>
            </a>
        </li>
        <hr class="sidebar-divider">
        <div class="sidebar-heading">
            GASTOS
        </div>

    <!---->
</ul>