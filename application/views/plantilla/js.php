<?= script_tag('vendor/jquery/jquery.min.js'); ?>
<?= script_tag('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>
<?= script_tag('vendor/jquery-easing/jquery.easing.min.js'); ?>
<?= script_tag('js/ruang-admin.min.js'); ?>
<?= script_tag('js/layout.js'); ?>
<?= script_tag('js/gijgo/gijgo.min.js'); ?>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApP9RAbkP4S5p_XZRcQfXUo5ATzSGqBGg"></script>

<script type="text/javascript">
    URLs = "<?= base_url('/') ?>";
    function sendRequest(url, data, method, cb) {
        opts = {};
        opts.url = url;
        if (data)
            opts.data = data;
        opts.type = method;
        opts.complete = cb;
        $.ajax(opts);
    }
</script>
<?= $scripts ?>