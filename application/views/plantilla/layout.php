<?php
if (!empty(auth()->idUsuario)) {
    ?>
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <?php $this->template->load_header(); ?>
        </head>
        <body id="page-top">
            <div id="wrapper">
                <!-- Sidebar -->
                <?php $this->template->load_menu(auth()->idUsuario); ?>
                <!-- Sidebar -->
                <div id="content-wrapper" class="d-flex flex-column">
                    <div id="content">
                        <!-- TopBar -->
                        <?php $this->load->view('plantilla/topbar.php') ?>
                        <!-- Topbar -->
                        <!-- Container Fluid-->
                        <main class="contenidos">
                            <?= $contents ?>
                        </main>
                        <!---Container Fluid-->
                    </div>
                    <!-- Footer -->
                    <?php $this->template->load_footer(); ?>
                    <!-- Footer -->
                </div>
            </div>
            <!-- Scroll to top -->
            <a class="scroll-to-top rounded" href="#page-top" style="background-color: #c90e14">
                <i class="fas fa-angle-up"></i>
            </a>
            <?php $this->load->view('plantilla/js.php'); ?>
            <script>
                URLs = "<?= base_url() ?>";
            </script>
        </body>
    </html>
    <?php
} else {
    $this->template->loadInicio('inicio/v_contenido');
}
?>

