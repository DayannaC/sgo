<div class="modal" id="modal_editar_datos">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder" id="nomElemento">EDITAR DATOS</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cerrarModalPermiso()">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <form class="form" method="GET" id="form_editar_datos">
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="nombres"><h4>Nombres</h4></label>
                                    <input type="text" class="form-control" name="nombres" id="nombres"  value="<?= auth()->nombres ?>" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="ape_paterno"><h4>Apellido Paterno</h4></label>
                                    <input type="text" class="form-control" name="ape_paterno" id="ape_paterno"  value="<?= auth()->ape_paterno ?>" required="">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="ape_materno"><h4>Apellido Materno</h4></label>
                                    <input type="text" class="form-control" name="ape_materno" id="ape_materno"  value="<?= auth()->ape_materno ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-6">
                                    <label for="correo"><h4>Correo</h4></label>
                                    <input type="email" class="form-control" name="correo" id="correo"  value="<?= auth()->correo ?>" required="">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">           
                        <button type="submit" id="editar_datos" name="editar_datos" class="btn btn-primary btn-user btn-block">Guardar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">           
                        <button type="button" id="doc_cerrar" class="btn btn-secondary" data-dismiss="modal" onclick="cerrarModalPermiso()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>