<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Mi Perfil</a></li>
        </ol>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-11 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="row">
                    <div class="col-sm-10 font-weight-bold text-center"><h1>Mi Perfil</h1></div>

                    <!--<div class="col-sm-10"><h1><?= ucwords(strtolower(auth()->nombre)) ?></h1></div>-->
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-deck">
                            <div class="card">
                                <div class="card-body shadow-lg">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div style="font-size: 80px;">
                                                <a data-toggle="modal" data-target="#modal_editar_datos" class="card-link"><i class="fas fa-user"></i> </a>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="p-3" style="font-size: 15;">
                                                <a data-toggle="modal" data-target="#modal_editar_datos" class="card-link">Editar Datos</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body shadow-lg">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div style="font-size: 80px;">
                                                <a data-toggle="modal" data-target="#modal_cambiar_clave" class="card-link"><i class="fas fa-lock"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="p-3" style="font-size: 15;">
                                                <a data-toggle="modal" data-target="#modal_cambiar_clave" class="card-link">Cambiar Clave</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>               
                            </div>
                            <div class="card">
                                <div class="card-body shadow-lg">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div style="font-size: 80px;">
                                                <a onclik="cargarApp()" data-toggle="modal" data-target="#modal_firmar"  class="card-link"><i class="fas fa-file-signature"></i></a>  
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="p-3" style="font-size: 15;">
                                                <a  onclik="cargarApp()" data-toggle="modal" data-target="#modal_firmar"  class="card-link">Firmar</a>
                                            </div>
                                        </div>
                                    </div>                
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-body shadow-lg">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div style="font-size: 70px;">
                                                <a data-toggle="modal" data-target="#modal_subir_firma" class="card-link"><i class="fas fa-signature"></i></a>
                                            </div>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="p-3" style="font-size: 15;">
                                                <a data-toggle="modal" data-target="#modal_subir_firma" class="card-link">Subir Firma</a>
                                            </div>
                                        </div>
                                    </div>                
                                </div>
                            </div>
                        </div>
                    </div><!--/tab-content-->
                </div><!--/col-9-->
            </div><!--/row-->
        </div><!--/row-->
    </div><!--/row-->
</div><!--/row-->

<?php $this->load->view('inicio/v_modal_editar_datos'); ?>
<?php $this->load->view('inicio/v_modal_cambiar_clave'); ?>
<?php $this->load->view('inicio/v_modal_firmar'); ?>
<?php $this->load->view('inicio/v_modal_subir_firma'); ?>
