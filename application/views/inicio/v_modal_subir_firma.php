<div class="modal" id="modal_subir_firma">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder" id="nomElemento">SUBIR FIRMA</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cerrarModalPermiso()">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">  
                            <img src="">
                            <form method="post" action="#" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="image">Nueva imagen</label>
                                    <input type="file" class="form-control-file" name="image" id="image">
                                </div>
                                <input type="button" class="btn btn-primary" id="subir_firma" value="Subir">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">           
                            <button type="button" id="doc_cerrar" class="btn btn-secondary" data-dismiss="modal" onclick="cerrarModalPermiso()">Cerrar</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>