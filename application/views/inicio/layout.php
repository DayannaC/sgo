<!DOCTYPE html>
<html lang="en">
    <head>
        <?php $this->load->view('plantilla/head.php'); ?>
        <?= link_tag('css/iniciar_sesion.css'); ?>
    </head>
    <body id="page-top">
        <div id="wrapper">
            <div id="content-wrapper" class="d-flex flex-column">
                <div id="content">
                    <main class="contenidos">
                        <?= $contents ?>
                    </main>
                </div>
            </div>
        </div>
        <?php $this->load->view('plantilla/js.php'); ?>
    </body>
</html>
