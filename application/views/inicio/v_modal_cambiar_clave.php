<div class="modal" id="modal_cambiar_clave">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder" id="nomElemento">CAMBIAR CLAVE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cerrarModalPermiso()">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <!--<form class="form" action="##" method="post" id="">-->
                        <div class="form-group">
                            <div class="col-xs-6">
                                <label for="clave"><h4>Contraseña Nueva</h4></label>
                                <input type="password" class="form-control" name="clave" id="clave">
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <label for="confirmar_clave"><h4>Confirmar Contraseña</h4></label>
                                <input type="password" class="form-control" name="confirmar_clave" id="confirmar_clave">
                            </div>
                        </div>
                        <!--</form>-->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">           
                        <button type="submit" id="cambiar_clave" name="cambiar_clave" class="btn btn-primary btn-user btn-block">Guardar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">           
                        <button type="button" id="doc_cerrar" class="btn btn-secondary" data-dismiss="modal" onclick="cerrarModalPermiso()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>