<div class="modal" id="modal_firmar">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder" id="nomElemento">FIRMAR</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cerrarModalPermiso()">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div id="signature-pad" class="signature-pad" style="height: 400px;">
                            <div class="signature-pad--body">
                                <canvas id="canvas"></canvas>
                            </div>
                            <div class="signature-pad--footer">
                                <div class="signature-pad--actions">
                                    <div>
                                        <button type="button" class="button clear" id="eliminar">Limpiar</button>
                                        <!--<button type="button" class="button" data-action="change-color">Change color</button>-->
                                        <button type="button" class="button" id="deshacer">Deshacer</button>
                                    </div>
                                    <div>
                                        <button type="button" class="button save" id="guardarPNG">Guardar como PNG</button>
                                        <button type="button" class="button save" id="guardarJPG">Guardar como JPG</button>
                                        <button type="button" class="button save" id="guardarSVG">Guardar como SVG</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">           
                        <button type="submit" id="firmar" name="firmar" class="btn btn-primary btn-user btn-block">Guardar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">           
                        <button type="button" id="doc_cerrar" class="btn btn-secondary" data-dismiss="modal" onclick="cerrarModalPermiso()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>