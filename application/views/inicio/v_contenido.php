<div class="grid-block" style="background-image: url('img/avion_edit.jpg'); width: 100%; height: 100vh; ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12 col-md-10 col-lg-9 col-xl-5 shadow-lg p-5 mb-5 my-5 bg-white rounded">
                <div class="text-center">
                    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
                        <div class="sidebar-brand-icon">
                            <img src="img/logo/logo_star.jpg">
                        </div>
                        <div class="sidebar-brand-text mx-3"></div>
                    </a>
                    <h1 class="h4 text-gray-900 mb-4">Iniciar Sesión</h1>
                </div>
                <?php
                $attributes = array('class' => 'user');
                echo form_open('Inicio/iniciar_sesion', $attributes)
                ?>
                <div class="form-group">
                    <input type="text" class="form-control" id="is_usuario" name="usuario" aria-describedby="" placeholder="Usuario">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" id="is_clave" name="clave" placeholder="Contraseña" maxlength="">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-block btn-ingresar" name="submit" value="Ingresar"/>
                </div>
                <?= form_close() ?>
                <!--                <div class="form-group">
                                    <input type="submit" class="btn btn-ingresar" name="submit" value="¿Olvidaste tu contraseña?"/>
                                    <input type="submit" class="btn btn-ingresar" name="submit" value="Registrarse"/>
                                </div>-->
                <hr>
                <div class="text-center">
                    <?php if (isset($error)): ?>
                        <p> <?= $error ?> </p>
                    <?php endif; ?>
                </div>
                <hr>
            </div>
        </div>
    </div>
</div>
