<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-1">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Liberacion de vuelo y/o despacho</a></li>
        </ol>
    </div>
    <div class="row">
        <div class="col-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">BRIEFING DE TRIPULACION / LIBERACION DE VUELO Y/O DESPACHO</h1>
                    <hr class="sidebar-divider">
                </div>
                <div class="container-fluid mb-1">
                    <div class="row">
                        <div class="col-lg-3 border">
                            <label>FECHA:</label>
                        </div>
                        <div class="col-lg-3 border">
                            <input id="datepicker"/>
                        </div>
                    </div>
                </div>
                <!-- INFORMACION DE VUELO-->
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12 border">
                            <h6 class="my-1 text-center font-weight-bold">INFORMACION DE VUELO</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 border">
                            <label>TOMA</label>
                        </div>
                        <div class="col-lg-2 border">
                            <label>SALA</label>
                        </div>
                        <div class="col-lg-2 border">
                            <label>TIPO AERONAVE</label>
                        </div>
                        <div class="col-lg-2 border">
                            <label>MATRICULA</label>
                        </div>
                        <div class="col-lg-2 border">
                            <label>N°VUELO</label>
                        </div>
                        <div class="col-lg-2 border">
                            <label>TRAMO</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                    </div>
                </div>
                <!-- TRIPULACION DE VUELO-->
                <div class="container-fluid my-1">
                    <div class="row">
                        <div class="col-lg-12 border">
                            <h6 class="my-1 text-center font-weight-bold">TRIPULACION DE VUELO</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 border">
                            <label>CMD</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>J/C</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 border">
                            <label>F/O</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>T/C</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 border">
                            <label>OBS</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>T/C</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-1 border" contenteditable="true">
                            <label>DSP</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                            <label>MEC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LIC.</label>
                        </div>
                        <div class="col-lg-2 border" contenteditable="true">
                        </div>
                    </div>
                </div>
                <!--INFORMACION OPERACIONAL-->
                <div class="container-fluid mb-1">
                    <div class="row">
                        <div class="col-lg-12 border">
                            <h6 class="my-1 text-center font-weight-bold">INFORMACION OPERACIONAL</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 border">
                            <label>RUTA</label>
                        </div>
                        <div class="col-lg-7 border" contenteditable="true">
                        </div>
                        <div class="col-lg-2 border">
                            <label>REGLA VLO.</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 border">
                            <label>ALTERNO DESPEGUE</label>
                        </div>
                        <div class="col-lg-3 border" contenteditable="true">
                        </div>
                        <div class="col-lg-3 border">
                            <label>ALTERNO DESTINO</label>
                        </div>
                        <div class="col-lg-3 border" contenteditable="true">
                        </div>
                    </div>
                </div>
                <!--PESOS OPERATIVOS - INFORMACION DE COMBUSTIBLE-->
                <div class="container-fluid mb-1">
                    <div class="row">
                        <div class="col-lg-6 border">
                            <h6 class="my-1 text-center font-weight-bold">PESOS OPERATIVOS</h6>
                        </div>
                        <div class="col-lg-6 border">
                            <h6 class="my-1 text-center font-weight-bold">INFORMACION DE COMBUSTIBLE</h6>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>TOTAL PAXS</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>PAX</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>CONSUMO AL DESTINO</label>
                        </div>
                        <div class="col-lg-1 border">
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>PESO DE PAXS</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>ALTERNO + HOLDING</label>
                        </div>
                        <div class="col-lg-1 border">
                            <label>+</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>PESO DE EQUIPAJE</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>MINIMO DE SALIDA</label>
                        </div>
                        <div class="col-lg-1 border">
                            <label>=</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>PESO DE CARGA</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>ADICIONAL</label>
                        </div>
                        <div class="col-lg-1 border">
                            <label>+</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>ZERO FUEL WEIGHT</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>TAKE OFF FUEL</label>
                        </div>
                        <div class="col-lg-1 border">
                            <label>=</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>TAKE OFF WEIGHT</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>RODAJE</label>
                        </div>
                        <div class="col-lg-1 border">
                            <label>+</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 border">
                            <label>LANDING WEIGHT</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                        <div class="col-lg-3 border">
                            <label>TOTAL ABORDO</label>
                        </div>
                        <div class="col-lg-1 border">
                            <label>=</label>
                        </div>
                        <div class="col-lg-1 border" contenteditable="true">
                        </div>
                        <div class="col-lg-1 border">
                            <label>LBS</label>
                        </div>
                    </div>
                </div>
                <!--INFORMACION DE DESPACHO - OBSERVACIONES-->
                <div class="container-fluid mb-1">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h6 class="my-1 text-center font-weight-bold">INFORMACION DE DESPACHO</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>1</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>PV/ATC</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>2</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>PV/OPERACIONAL</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>3</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>METAR</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>4</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>TAFS</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>5</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>NOTAMS</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>6</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>FOTO SAT.</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>7</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>CARTA DE VIENTOS</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>8</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>NOTOC</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>9</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>DIFERIDOS</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>10</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>PTO.DE RECARGA</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-2 border">
                                    <label>11</label>
                                </div>
                                <div class="col-lg-8 border">
                                    <label>OTRO</label> 
                                </div>
                                <div class="col-lg-2 border" contenteditable="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-1">

                        </div>
                        <div class="col-lg-7">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label></label>
                                </div>
                            </div>
                            <div class="row border">
                                <div class="col-lg-12 border-bottom">
                                    <label class="font-weight-bold">OBSERVACIONES:</label>

                                </div>
                                <div class="col-lg-12">
                                    <textarea class="border-0" style="height: 335px;width: 550px;"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>      
                </div>
                <!--FIRMAS-->
                <div class="container-fluid my-5">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>FIRMA CMD:</label>
                                </div>
                                <div class="col-lg-8 border-bottom" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>NOMBRE:</label>
                                </div>
                                <div class="col-lg-8 border-bottom" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>LIC.</label>
                                </div>
                                <div class="col-lg-8 border-bottom" contenteditable="true">
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                        </div>
                        <div class="col-lg-5">
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>FIRMAD.V.</label>
                                </div>
                                <div class="col-lg-8 border-bottom" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>NOMBRE:</label>
                                </div>
                                <div class="col-lg-8 border-bottom" contenteditable="true">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>LIC.</label>
                                </div>
                                <div class="col-lg-8 border-bottom" contenteditable="true">
                                </div>
                            </div>
                        </div>
                    </div>      
                </div>
                <!--                <div class="card">
                                    <div class="card-body">
                                        CERTIFICO QUE ESTE VUELO ES LIBERADO DE ACUERDO CON LAS RAPS : 121-345,2215,2220,2520 a-b, 2525,2530, 
                                        2555,2565,2575,2585,2680,2705,2715 Y 2725, CON CONOCIMIENTO DE LA RUTA ,ALTERNOS , COMBUSTIBLE 										
                                        MINIMOS, REPORTES METEOROLOGICOS, NOTAMS Y PRONOSTICOS, AERONAVE LIBERADA DE ACUERDO AL MEL/CDL
                                    </div>
                                </div>-->
<!--                <canvas id="canvas">Su navegador no soporta canvas :( </canvas>
                <p id="limpiar">limpiar canvas</p>-->
                <div class="row">
                    <div class="col-lg-12 box-footer">
                        <button type="submit" id="" name="agregar_usuario" class="btn btn-primary btn-user btn-block">GUARDAR</button>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
