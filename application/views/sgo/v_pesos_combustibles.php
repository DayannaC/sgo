<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Tabla de pesos y combustibles</a></li>
        </ol>
    </div>
    <div class="row ">
        <div class="col-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">TABLA DE PESOS Y COMBUSTIBLES</h1>
                    <hr class="sidebar-divider">
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <label for="">VERSION:</label>
                        <input type="text" name="version" class="form-control form-control-user" maxlength="3" required="" onkeyup="convertirMayuscula(this)" />
                    </div>
                    <div class="form-group">
                        <label for="">RUTA:</label>
                        <input type="text" name="ruta" class="form-control form-control-user" maxlength="3" required="" onkeyup="convertirMayuscula(this)" />
                    </div>
                    <div class="form-group">
                        <label for="">PAX:</label>
                        <input type="text" name="pax" class="form-control form-control-user" maxlength="3" required="" onkeyup="convertirMayuscula(this)" />
                    </div>
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">KGS</th>
                            <th scope="col">LBS</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <th scope="row">DOW</th>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                          </tr>
                          <tr>
                            <th scope="row">PYL</th>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                          </tr>
                          <tr>
                            <th scope="row">ZERO FUEL</th>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                          </tr>
                          <tr>
                            <th scope="row">FUEL</th>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                          </tr>
                          <tr>
                            <th scope="row">TOW</th>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                          </tr>
                          <tr>
                            <th scope="row">LDG</th>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                            <td><input type="text" name="" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/></td>
                          </tr>
                        </tbody>
                     </table>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">TRIM/MAC:</label>
                            <input type="text" name="trim_mac" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">N1 REF:</label>
                            <input type="text" name="n1_ref" id="clave" class="form-control form-control-user" maxlength="4" required="" onkeyup="convertirMayuscula(this)">
                        </div>
                    </div>   
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">FLAPS T/O:</label>
                            <input type="text" name="trim_mac" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">N1 REF:</label>
                            <input type="text" name="n1_ref" id="clave" class="form-control form-control-user" maxlength="4" required="" onkeyup="convertirMayuscula(this)">
                        </div>
                    </div>   
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">FLAPS LDG:</label>
                            <input type="text" name="trim_mac" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">V1:</label>
                            <input type="text" name="n1_ref" id="clave" class="form-control form-control-user" maxlength="4" required="" onkeyup="convertirMayuscula(this)">
                        </div>
                    </div>   
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">HORA:</label>
                            <input type="text" name="trim_mac" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">VR:</label>
                            <input type="text" name="n1_ref" id="clave" class="form-control form-control-user" maxlength="4" required="" onkeyup="convertirMayuscula(this)">
                        </div>
                    </div>   
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="">FECHA:</label>
                            <input type="text" name="trim_mac" class="form-control form-control-user" required=""  onkeyup="convertirMayuscula(this)"/>
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">V2:</label>
                            <input type="text" name="n1_ref" id="clave" class="form-control form-control-user" maxlength="4" required="" onkeyup="convertirMayuscula(this)">
                        </div>
                    </div>   
                    <div class="row">
                        <div class="form-group col-sm-6">
                        </div>
                        <div class="form-group col-sm-6">
                            <label for="">T°:</label>
                            <input type="text" name="n1_ref" id="clave" class="form-control form-control-user" maxlength="4" required="" onkeyup="convertirMayuscula(this)">
                        </div>
                    </div>   
                    <div class="box-footer">
                        <button type="submit" id="agregar_usuario" name="agregar_usuario" class="btn btn-primary btn-user btn-block">Enviar</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>