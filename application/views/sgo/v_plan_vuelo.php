<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Plan de vuelo / Flight plan</a></li>
        </ol>
    </div>
    <div class="row ">
        <div class="col-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">PLAN DE VUELO / FLIGHT PLAN</h1>
                    <hr class="sidebar-divider">
                </div>
                <div class="box-body">
                    <div class="container-fluid border border-dark mb-1">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">PRIORIDAD</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="">Priority</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">FF --></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold"></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label>HORA DE DEPOSITO</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="">FILLING TIME</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-6 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-7">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">DESTINATARIO</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="">Adresses(s)</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="">REMITENTE</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="">ORIGINATOR</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>   
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="font-weight-bold mb-0">IDENTIFICACION EXACTA DEL ( DE LOS ) DESTINATARIO (OS) Y/O REMITENTE</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <label class="">Specific identification of adresse (s) and/or Originator</label>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark my-3">
                        <div class="row mt-1">
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">3.TIPO DE MENSAJE</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Message Type</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">FPL</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">7. IDENTIFICACION DE AERONAVE </h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Aircraft Identification</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">8. REGLAS DE VUELO</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Flight Rules</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">TIPO DE VUELO</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Type of Flight</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>      
                        <div class="row mt-2">
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">9.NUMERO</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Number</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">TIPO DE AERONAVE</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Type of aircraft</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">CAT.DE ESTELA TURBULENTA</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Wake Turbulence Cat</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">10.EQUIPO</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Equipment</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-2">
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">13.AERODROMO DE SALIDA</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Departure Aerodrome</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">HORA</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Time</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                            </div>
                        </div>
                        <div class="row my-2">
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">15.VELOCIDAD DE CRUCERO</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Crusing Speed</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">NIVEL</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Level</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">--></label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">RUTA</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Route</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark my-3">
                        <div class="row mt-1">
                            <div class="col-lg-12">
                                <h6 class="font-weight-bold border-bottom text-center">OPROS2F OPROS UT317 IRESO PUMAS</h6>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">18.AERODROMO DE DESTINO </h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Destination Aerodrome</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">EET TOTAL</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Total EET</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>HR MIN</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">AERÓDROMO ALTN</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>ALTN Aerodrome</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">2° AERÓDROMO ALTN</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>2 ALTN Aerodrome</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>      
                        <div class="row mt-2">
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6 class="font-weight-bold">18.OTROS DATOS</h6>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h6>Other Informations</h6>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 border">
                                <h6 class="text-center font-weight-bold">OPERADOR STAR PERU</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 border">
                                <h6 class="text-center font-weight-bold">PBN/ B1 O1 D1</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 border">
                                <h6 class="text-center font-weight-bold">NAV / GNSS</h6>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 border">
                                <label class="font-weight-bold">DOF / 200708 MCA N-1070 / TRANSPORTE AEREO ESPECIAL R.M / 232 2020 D.S / 044 2020</label>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark my-3">
                        <div class="row mt-1">
                            <div class="col-lg-12">
                                <h6 class="font-weight-bold border-bottom text-center">INFORMACIONES SUPLEMENTARIAS (EN LOS MENSAJES FPL NO HAY QUE TRANSMITIR ESTOS DATOS )</h6>
                            </div>
                        </div>
                        <div class="row mb-1">
                            <div class="col-lg-12">
                                <h6 class="font-weight-bold border-bottom text-center">Suplementary Information (not to be Transmited in FPL message)</h6>
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">--> D /</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold mb-0">BOTES/Dinghies</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">NUMERO</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">Number</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold ml-2">----></label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold m-0">CAPACIDAD</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">Capacity</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold ml-2">---></label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold m-0">CUBIERTA</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">C /</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold ml-2">----></label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold m-0">COLOR</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">Collor</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold pl-4">A /</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold m-0">COLOR Y MARCAS DE LA AERONAVE</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">Aircraf collor and Marking</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold">--> N /</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold m-0">OBSERVACIONES</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">Remark</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>
                        <div class="row mt-1">
                            <div class="col-lg-1">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label></label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <label class="font-weight-bold pl-4">C /</label>
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="font-weight-bold m-0">PILOTO AL MANDO</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 pl-0">
                                        <label class="m-0">Pilot in Command</label>
                                    </div> 
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 border" contenteditable="true">
                                    </div> 
                                </div>
                            </div>
                            <div class="col-lg-2">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 box-footer">
                            <button type="submit" id="" name="agregar_usuario" class="btn btn-primary btn-user btn-block">GUARDAR</button>
                        </div>
                    </div>
                </div>
            </div>