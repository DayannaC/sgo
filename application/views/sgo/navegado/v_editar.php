<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-1">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item"><a href="<?= base_url('sgo/planVueloNavegado') ?>">Plan de Vuelo Navegado</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Editar Plan de Vuelo Navegado</a></li>
        </ol>
    </div>
    <div class="row">
        <div class="col-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">EDITAR PLAN DE VUELO NAVEGADO</h1>
                    <hr class="sidebar-divider">
                </div>
                <div class="container-fluid mb-1">
                    <div class="row">
                        <div class="col-lg-5">
                            <section class="section row fuel-weight-tables mt-10">
                                <table class="">
                                    <thead>
                                        <tr>
                                            <th title="id" hidden></th>
                                            <th title="Fuel">Fuel</th>
                                            <th title="fuelUnit" class="text-right">(lbs)</th>
                                            <th title="Time" class="text-right bold">Time</th>
                                            <th title="Time" class="text-right bold">Actuals</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($res->fuel as $key => $fuel): ?>
                                            <tr>
                                                <td id="tnavegable_id" hidden=""><?= $fuel->tnavegable_id ?></td>
                                                <td id="fuel"><?= $fuel->fuel ?></td>
                                                <td class="text-right" id="lbs-<?= $fuel->fuel ?>-tfuel"><?= $fuel->lbs ?></td>
                                                <td class="text-right bold"><?= $fuel->time ?></td>
                                                <td class="text-right" contenteditable="true" onblur="editarTFuel(this)"><?= $fuel->actuals ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                                <!--</div>--> 
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <table class="alt-checks">
                                <thead>
                                    <tr>
                                        <th title="Alternate Checks">Alt/Rvsm checks</th>
                                        <th title="Alternate 1">Alt #1</th>
                                        <th title="Alternate 2">Alt #2</th>
                                        <th title="Stand-by">Stand-by</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="4" class="tall">RAMP</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="tall" id="<?= $res->id ?>" style="background: #faebd794; height: 35px; padding-left: 5px; padding-top: 5px;" onblur="editarTNavegable(this, 'ramp')" class="form-control" rows="4" contenteditable=""><?= $res->ramp ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6">
                            <table class="notes-table">
                                <thead>
                                    <tr class="notesHeaderRow">
                                        <th title="Notes" colspan="6">Notes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($res->notes as $key => $note): ?>
                                        <tr>
                                            <td id="tnavegable_id_notes" ><?= $note->tnavegable_id ?></td>
                                            <td class="tall"><?= $note->fila1 ?></td>
                                            <td><span contenteditable type="text" <?= $note->fila3 !== 'Fuel used:' ? 'data-inputmask-alias="datetime" data-inputmask-inputformat="HH:MM" data-inputmask-placeholder="hh:mm" placeholder="hh:mm"' : ''?> class="<?= $note->fila1 == 'Off:' ? 'off' : '' ?> <?= ($note->fila3 !== 'Fuel used:') ? 'timepicker' : 'fuel' ?> " id="<?= $note->fila2?>-<?php $cadena3=explode(' ',$note->fila3); echo $cadena3[0]; ?>-valor1-<?= $note->tnavegable_id?>"><?= $note->valor1 ?></td>
                                            <td class="tall"><?= $note->fila2 ?></td>
                                            <td><span contenteditable type="text" <?= $note->fila3 !== 'Fuel used:' ? 'data-inputmask-alias="datetime" data-inputmask-inputformat="HH:MM" data-inputmask-placeholder="hh:mm" placeholder="hh:mm"' : ''?> class="<?= ($note->fila3 !== 'Fuel used:') ? 'timepicker' : 'fuel' ?> border-0" id="<?= $note->fila2?>-<?php $cadena3=explode(' ',$note->fila3); echo $cadena3[0]; ?>-valor2-<?= $note->tnavegable_id?>"><?= $note->valor2 ?></span></td>
                                            <td class="tall bold"><?= $note->fila3 ?></td>
                                            <td><span class="border-0 <?php $cadena3=explode(' ',$note->fila3); echo $cadena3[0]; ?>" id="valor3" type="text"  value="<?= $note->valor3 ?>" style="color:darkred" disabled="true"><?= $note->valor3 ?></span></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <section class="section row clearance notes mt-10">
                                <div class="dont-break-container col-xs-12 mb-10">
                                    <div class="writing-line">
                                        <strong>Departure ATIS</strong>
                                    </div>
                                    <table>
                                        <tbody>
                                            <tr><td id="<?= $res->id ?>" style="background: #faebd794; height: 35px; padding-left: 5px; padding-top: 5px;" contenteditable onblur="editarTNavegable(this, 'departureatis')"><?= $res->departureatis ?></td></tr>
                                        </tbody>
                                    </table>
                                    <div class="writing-line"></div>
                                </div>
                                <div class="dont-break-container col-xs-12">
                                    <div class="writing-line">
                                        <strong>Clearance</strong>
                                    </div>
                                    <table>
                                        <tbody>
                                            <tr><td id="<?= $res->id ?>" style="background: #faebd794; height: 35px; padding-left: 5px; padding-top: 5px;" contenteditable="true" onblur="editarTNavegable(this, 'clearance')"><?= $res->clearance ?></td></tr>
                                        </tbody>
                                    </table>
                                    <div class="writing-line"></div>
                                </div>
                            </section>
                        </div>
                    </div>
                    <input type="text" class="hdp">
                    <input type="text" class="hdp">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="way-point truta mt-20 text-centered show-borders">
                                    <thead class="multi-row">
                                        <tr style="display: none;">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        <tr class="waypoint-header-row1">
                                            <th colspan="2"></th>
                                            <th title="Airway">AWY</th>
                                            <th title="Wind">WIND</th>
                                            <th title="Magnetic heading">MAG</th>
                                            <th></th>
                                            <th title="Distance">DIST</th>
                                            <th title="Fuel Pounds" colspan="3">FUEL lbs</th>
                                            <th title="Times" colspan="3">TIMES</th>
                                            <th title="Remarks" colspan="3" class="remarks">REMARKS</th>
                                        </tr>
                                        <tr class="waypoint-header-row2">
                                            <th title="Waypoint" colspan="2" class="text-left">Waypoint</th>
                                            <th title="Altitude">ALT</th>
                                            <th title="Wind direction / Wind speed">DIR/SPD</th>
                                            <th title="Heading">HDG</th>
                                            <th title="True airspeed">TAS</th>
                                            <th title="Remaining distance">LEG</th>
                                            <th title="Remaining fuel">REM</th>
                                            <th title="Fuel used">USED</th>
                                            <th title="Fuel flow">FLOW</th>
                                            <th title="Time to fly through leg">LEG</th>
                                            <th title="Estimated time enroute">ETE</th>
                                            <th title="Estimated time of arrival">ETA</th>
                                            <th colspan="3"></th>
                                        </tr>
                                        <tr class="waypoint-header-row3">
                                            <th title="Coordinates" colspan="2" class="text-left normal">Coordinates</th>
                                            <th title="Minimum safe altitude">MSA</th>
                                            <th title="ISA deviation">ISA</th>
                                            <th title="Course">CRS</th>
                                            <th title="Ground airspeed">GS</th>
                                            <th title="Distance remaining">REM</th>
                                            <th></th>
                                            <th title="Actual fuel used">ACTL</th>
                                            <th></th>
                                            <th title="Time remaining">REM</th>
                                            <th title="Actual time enroute">ATE</th>
                                            <th title="Actual time of arrival" class="ata">ATA</th>
                                            <th colspan="3"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <span><?php // var_dump(count($res->rutas));?></span>
                                        <?php $classTD = ''; $classAlt = ''; $classFila12 = 0; $banTD = 0; ?>
                                        <?php foreach ($res->rutas as $key => $ruta): ?>
                                            <?php if ($ruta->columna == 1): ?>
                                                <tr class="table-data-row <?= $classTD ?> alt<?= $banTD ?>" id="fila_ruta-<?= $ruta->tnavegable_id . '-' . $ruta->id ?>">
                                                    <td id="tnavegable_id_rutas" hidden=""><?= $ruta->tnavegable_id . '-' . $ruta->id ?></td>
                                                    <td colspan="2"><?= $ruta->fila1 ?></td>
                                                    <td><?= $ruta->fila2 ?></td>
                                                    <td><?= $ruta->fila3 ?></td>
                                                    <td><?= $ruta->fila4 ?></td>
                                                    <td><?= $ruta->fila5 ?></td>
                                                    <td><?= $ruta->fila6 ?></td>
                                                    <td id="rem-<?= $ruta->tnavegable_id . '-' . $ruta->id ?>" class="">
                                                        <?php $fila7 = explode('<br>', $ruta->fila7); ?>
                                                        <span style="margin-top: 5px; margin-left: 5px; display: table;"><?= $fila7[0] ?></span>
                                                        <span class="fila7span fila7 <?= $classTD ?>" style="display: block; padding: 5px; background: antiquewhite;" id="rem-<?= $ruta->tnavegable_id . '-' . $ruta->id ?>" contenteditable><?= count($fila7) == 2 ? $fila7[1] : '' ?></span>
                                                    </td>
                                                    <td id="actl-<?= $ruta->tnavegable_id . '-' . $ruta->id ?>" class="fila8"><?= $ruta->fila8 ?></td>
                                                    <td id="flow" class="fila9"><?= $ruta->fila9 ?></td>
                                                    <td id="remaining" class="fila10"><?= $ruta->fila10 ?></td>
                                                    <td id="ate" class="fila11" name="<?= $ruta->tnavegable_id . '-' . $ruta->id ?>">
                                                        <?= $ruta->fila11 ?>
                                                    </td>
                                                    <td id="ata">
                                                        <?php $fila12 = explode('<br>', $ruta->fila12); ?>
                                                        <span id="<?= $ruta->tnavegable_id . '-' . $ruta->id ?>" style="margin-top: 5px; margin-left: 5px; display: table;"><?= $classFila12 == 0 ? $fila12[0] : '' ?></span>
                                                        <?php if ($key != 0): ?>
                                                            <span contenteditable="" style="background: #f5efe7; font-size: 12px; height: 2em; width: 4em; padding-left: 5px;" class="fila12 <?= $classTD ?> <?= $classAlt ?>" id="<?= $ruta->tnavegable_id . '-' . $ruta->id ?>" data-inputmask-alias="datetime" data-inputmask-inputformat="HH:MM" data-inputmask-placeholder="hh:mm" placeholder="hh:mm"><?=  $classFila12 == 1 ? $fila12[0] : (count($fila12) == 2 ? $fila12[1] : '') ?></span>
                                                        <?php endif ?>
                                                        <?php $classFila12 = 0; $classAlt = '';?>
                                                    </td>
                                                    <td colspan="3" contenteditable="" id="<?= $ruta->tnavegable_id . '-' . $ruta->id ?>" class="fila13" style="background: #fff3e4;"><?= $ruta->fila13 ?></td>
                                                </tr>
                                            <?php else: ?>
                                                <?php
                                                    $classTD = 'alt';
                                                    $classAlt = 'horaAlt';
                                                    $classFila12 = 1;
                                                    $banTD++;
                                                    if ($banTD == 1) {
                                                        $nombreArribal = 'Arrival ATIS';
                                                        $valorArribal = $res->arrivalatis;
                                                        $classArribal = 'arrivalatis';
                                                    }
                                                    elseif ($banTD == 2) {
                                                        $nombreArribal = 'Alternate 1 Arrival ATIS';
                                                        $valorArribal = $res->arrivalatis2;
                                                        $classArribal = 'arrivalatis2';
                                                    }
                                                ?>
                                                <tr class="">
                                                    <td><b><em><?= $nombreArribal ?></em></b></td>
                                                </tr>
                                                <tr class="">
                                                    <td id="<?= $res->id ?>" colspan="13" style="background: #faebd794; height: 35px; padding-left: 5px; padding-top: 5px;" contenteditable="true" onblur="editarTNavegable(this, '<?= $classArribal ?>')"><?= $valorArribal ?></td>
                                                </tr>
                                                <tr class="sub-header ban<?= $banTD?>"  id="alternate_fila_ruta-<?= $ruta->tnavegable_id . '-' . $ruta->id ?>">
                                                    <td title="Alternate 1 Route" class="text-left" id="alternateRoute" colspan="19"><?= $ruta->fila1 ?></td>
                                                </tr>
                                            <?php endif ?>
                                        <?php endforeach ?>
                                        <?php
                                            $banTD++;
                                            if ($banTD == 2) {
                                                $nombreArribal = 'Alternate 1 Arrival ATIS';
                                                $valorArribal = $res->arrivalatis2;
                                                $classArribal = 'arrivalatis';
                                            }
                                            elseif ($banTD == 3) {
                                                $nombreArribal = 'Alternate 2 Arrival ATIS';
                                                $valorArribal = $res->arrivalatis3;
                                                $classArribal = 'arrivalatis3';
                                            }
                                        ?>
                                        <tr class="">
                                            <td><b><em><?= $nombreArribal ?></em></b></td>
                                        </tr>
                                        <tr class="">
                                            <td id="<?= $res->id ?>" colspan="13" style="background: #faebd794; height: 35px; padding-left: 5px; padding-top: 5px;" contenteditable="true" onblur="editarTNavegable(this, '<?= $classArribal ?>')"><?= $valorArribal ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="row float-right">
                        <button class="btn btn-danger btn-without-connection">Guardar Cambios</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>












