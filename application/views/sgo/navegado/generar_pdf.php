<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=960, initial-scale=1">
                <title>ForeFlight International Navlog</title>
                <script>
                    function setTheme(theme) {
                        var body = document.getElementsByTagName('body');
                        body[0].className = theme;
                        return 'Navlog theme set to ' + theme;
                    }
                </script>
                <?= link_tag('css/sgo/ver.css') ?>
                </head>
                <body>
                    <div id="wrapper">
                        <div class="container-fluid">
                            <section class="row section navlog-header">
                                <div class="col-xs-8 flight-summary">
                                    <strong>
                                        <?= $res->titulo ?>
                                    </strong>
                                    <div class="perf-summary">
                                        <?= $res->subtitulo ?>
                                    </div>
                                </div>
                                <div class="col-xs-4 row text-left date-created">
                                    <?= $res->textofecha ?>
                                </div>
                            </section>

                            <div class="hr"></div>

                            <section class="row section performance-summary">
                                <table>
                                    <tbody>
                                        <tr class="no-border">
                                            <td class="performance-metric ete">
                                                <strong>ETE</strong>
                                                <span><?= $res->ete ?></span>
                                            </td>
                                            <td class="performance-metric distance">
                                                <strong>Distance</strong>
                                                <span><?= $res->distance ?></span>
                                            </td>
                                            <td class="performance-metric avg-wind">
                                                <strong>Avg Wind</strong>
                                                <span class="no-wrap"><?= $res->avgwind ?></span>
                                            </td>
                                            <td class="performance-metric etd">
                                                <strong>ETD</strong>
                                                <span><?= $res->etd ?></span>
                                            </td>
                                            <td class="performance-metric eta">
                                                <strong>ETA</strong>
                                                <span><?= $res->eta ?></span>
                                            </td>
                                            <td class="performance-metric avg-tas">
                                                <strong>Avg TAS</strong>
                                                <span><?= $res->avgtas ?></span>
                                            </td>
                                            <td class="performance-metric altitude">
                                                <strong>Altitude</strong>
                                                <span><?= $res->altitude ?></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </section>

                            <div class="hr"></div>

                            <section class="row section route">
                                <div class="col-xs-12">
                                    <strong>Route</strong>
                                    <div>
                                        <?= $res->route ?>
                                    </div>
                                </div>
                            </section>

                            <section class="section row fuel-weight-tables mt-10">
                                <div class="col-xs-4">
                                    <table class="fuel">
                                        <thead>
                                            <tr >
                                                <th title="Fuel" class="border-bottom">Fuel</th>
                                                <th title="fuelUnit" class="text-right border-bottom">(lbs)</th>
                                                <th title="Time" class="text-center bold border-bottom">Time</th>
                                                <th title="Time" class="text-right bold border-bottom ">Actuals</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->fuel as $key => $fuel): ?>
                                                <tr>
                                                    <td class="border-bottom"><?= $fuel->fuel ?></td>
                                                    <td class="text-right border-bottom"><?= $fuel->lbs ?></td>
                                                    <td class="text-center bold border-bottom"><?= $fuel->time ?></td>
                                                    <td class="border-bottom"><?= $fuel->actuals ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-4">
                                    <table class="weights">
                                        <thead>
                                            <tr>
                                                <th title="Weights in pounds">Weights (lbs)</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->weight as $key => $weight): ?>
                                                <tr>
                                                    <td><?= $weight->weight ?></td>
                                                    <td class="text-right bold"><?= $weight->numero ?></td>
                                                </tr>

                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-xs-4">
                                    <table class="info">
                                        <thead>
                                            <tr>
                                                <th title="Info"colspan="2">Info</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->info as $key => $info): ?>
                                                <tr>
                                                    <td><?= $info->info ?></td>
                                                    <td class="bold"><?= $info->item ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>

                            <section class="row section tables-wrapper mt-10">
                                <div class="col-xs-6">
                                    <table class="alt-checks">
                                        <thead>
                                            <tr>
                                                <th title="Alternate Checks">Alt/Rvsm checks</th>
                                                <th title="Alternate 1">Alt #1</th>
                                                <th title="Alternate 2">Alt #2</th>
                                                <th title="Stand-by">Stand-by</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4" class="tall">RAMP</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="tall" class="form-control" rows="4" contenteditable=""><?= $res->ramp ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-6">
                                    <table class="notes-table">
                                        <thead>
                                            <tr class="notesHeaderRow">
                                                <th title="Notes" colspan="3">Notes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->notes as $key => $note): ?>
                                                <tr>
                                                    <td class="tall"><?= $note->fila1 ?> <?= $note->valor1 ?></td>
                                                    <td class="tall"><?= $note->fila2 ?> <?= $note->valor2 ?></td>
                                                    <td class="tall bold"><?= $note->fila3 ?> <?= $note->valor3 ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>
                            <br><br><br>
                                        <section class="row section tables-wrapper mt-10">
                                            <div class="col-xs-12">
                                                <table class="">
                                                    <thead>
                                                        <tr class="">
                                                            <th title="Notes" class="border-bottom">Departure ATIS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="tall border"><?= $res->departureatis ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
                                        <section class="row section tables-wrapper mt-10">
                                            <div class="col-xs-12">
                                                <table class="">
                                                    <thead>
                                                        <tr class="">
                                                            <th title="" class="border-bottom">Clearance</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="tall border-bottom"><?= $res->clearance ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
            <!--                            <section class="section row clearance notes mt-10">
                                            <div class="col-xs-12 dont-break-container">
                                                <div class="writing-line">
                                                    <strong>Clearance</strong>
                                                </div>
                                                <div class="writing-line"><?= $res->clearance ?></div>
                                                <div class="writing-line"></div>
                                            </div>
                                        </section>-->

                                        <table class="way-point mt-20 text-centered show-borders">
                                            <thead class="multi-row">
                                                <tr style="display: none;">
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                                <tr class="waypoint-header-row1">
                                                    <th colspan="2"></th>
                                                    <th title="Airway">AWY</th>
                                                    <th title="Wind">WIND</th>
                                                    <th title="Magnetic heading">MAG</th>
                                                    <th></th>
                                                    <th title="Distance">DIST</th>
                                                    <th title="Fuel Pounds" colspan="3">FUEL lbs</th>
                                                    <th title="Times" colspan="3">TIMES</th>
                                                    <th title="Remarks" colspan="3" class="remarks">REMARKS</th>
                                                </tr>
                                                <tr class="waypoint-header-row2">
                                                    <th title="Waypoint" colspan="2" class="text-left">Waypoint</th>
                                                    <th title="Altitude">ALT</th>
                                                    <th title="Wind direction / Wind speed">DIR/SPD</th>
                                                    <th title="Heading">HDG</th>
                                                    <th title="True airspeed">TAS</th>
                                                    <th title="Remaining distance">LEG</th>
                                                    <th title="Remaining fuel">REM</th>
                                                    <th title="Fuel used">USED</th>
                                                    <th title="Fuel flow">FLOW</th>
                                                    <th title="Time to fly through leg">LEG</th>
                                                    <th title="Estimated time enroute">ETE</th>
                                                    <th title="Estimated time of arrival">ETA</th>
                                                    <th colspan="3"></th>
                                                </tr>
                                                <tr class="waypoint-header-row3">
                                                    <th title="Coordinates" colspan="2" class="text-left normal border-bottom">Coordinates</th>
                                                    <th title="Minimum safe altitude" class="border-bottom">MSA</th>
                                                    <th title="ISA deviation" class="border-bottom">ISA</th>
                                                    <th title="Course" class="border-bottom">CRS</th>
                                                    <th title="Ground airspeed" class="border-bottom">GS</th>
                                                    <th title="Distance remaining" class="border-bottom">REM</th>
                                                    <th class="border-bottom"></th>
                                                    <th title="Actual fuel used" class="border-bottom">ACTL</th>
                                                    <th class="border-bottom"></th>
                                                    <th title="Time remaining" class="border-bottom">REM</th>
                                                    <th title="Actual time enroute" class="border-bottom">ATE</th>
                                                    <th title="Actual time of arrival" class="ata border-bottom">ATA</th>
                                                    <th colspan="3"></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($res->rutas as $key => $ruta): ?>
                                                    <?php if ($ruta->columna == 1): ?>
                                                        <tr class="table-data-row">
                                                            <td colspan="2"  class="border-bottom"><?= $ruta->fila1 ?></td>
                                                            <td  class="border-bottom"><?= $ruta->fila2 ?></td>
                                                            <td  class="border-bottom"><?= $ruta->fila3 ?></td>
                                                            <td  class="border-bottom"><?= $ruta->fila4 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila5 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila6 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila7 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila8 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila9 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila10 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila11 ?></td>
                                                            <td class="border-bottom"><?= $ruta->fila12 ?></td>
                                                            <td colspan="3"  class="border-bottom"><?= $ruta->fila13 ?></td>
                                                        </tr>
                                                    <?php else: ?>
                                                        <tr class="sub-header">
                                                            <td title="Alternate 1 Route" class="text-left" id="alternateRoute" colspan="19"><?= $ruta->fila1 ?></td>
                                                        </tr>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                        <section class="row section tables-wrapper mt-10">
                                            <div class="dont-break-container col-xs-12 mb-10">
                                                <table class="">
                                                    <thead>
                                                        <tr class="">
                                                            <th title="" class="border-bottom">Arrival ATIS</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="tall border-bottom"><?= $res->arrivalatis ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </section>
            <!--                            <section class="section row clearance notes mt-10">
                                            <div class="dont-break-container col-xs-12 mb-10">
                                                <div class="writing-line">
                                                    <strong>Arrival ATIS</strong>
                                                </div>
                                                <div class="writing-line"><?= $res->arrivalatis ?></div>
                                                <div class="writing-line"></div>
                                            </div>
                                        </section>-->

                                        <table class="alt-advisor show-borders mt-20">
                                            <thead class="multi-row">
                                                <tr class="alt-advisor-header row1">
                                                    <th colspan="2"></th>
                                                    <th title="Performance profile" class="text-centered" colspan="2">Long Range Cruise (PLAN)</th>
                                                    <th title="Performance profile" class="text-centered" colspan="2">Mach 0.82</th>
                                                    <th title="Performance profile" class="text-centered" colspan="2">Mach 0.80</th>
                                                </tr>
                                                <tr class="alt-advisor-header row2">
                                                    <th title="Altitude" class="border-bottom">Altitude</th>
                                                    <th title="Average wind" class="border-right border-bottom text-centered">Avg wind</th>
                                                    <th title="Time" class="text-left normal border-bottom">TIME</th>
                                                    <th title="Fuel" class="text-right border-right border-bottom normal">FUEL</th>
                                                    <th title="Time" class="text-left normal border-bottom">TIME</th>
                                                    <th title="Fuel" class="text-right border-right border-bottom normal">FUEL</th>
                                                    <th title="Time" class="text-left normal border-bottom">TIME</th>
                                                    <th title="Fuel" class="text-right normal border-bottom">FUEL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($res->latitudes as $key => $latitude): ?>
                                                    <tr class="alt-advisor-data">
                                                        <td class="border-bottom"><?= $latitude->fila1 ?></td>
                                                        <td class="text-centered border-bottom"><?= $latitude->fila2 ?></td>
                                                        <td class="border-bottom"><?= $latitude->fila3 ?></td>
                                                        <td class="text-right border-bottom"><?= $latitude->fila4 ?></td>
                                                        <td class="border-bottom"><?= $latitude->fila5 ?></td>
                                                        <td class="text-right border-bottom"><?= $latitude->fila6 ?></td>
                                                        <td class="border-bottom "><?= $latitude->fila7 ?></td>
                                                        <td class="text-right border-bottom"><?= $latitude->fila8 ?></td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </tbody>
                                        </table>
                                        <br><br><br>
                                                    <section class="section row flight-information-region mt-20">
                                                        <div class="col-xs-6">
                                                            <table class="flight-information-region show-borders text-centered">
                                                                <thead>
                                                                    <tr class="table-header-row">
                                                                        <th title="Flight information region" class="text-left">Flight Information Region</th>
                                                                        <th title="Estimated entry time">EET</th>
                                                                        <th title="Entry">ENTRY</th>
                                                                        <th title="Exit">EXIT</th>
                                                                        <th title="Distance">DISTANCE</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php foreach ($res->flight_1 as $key => $flight_1): ?>
                                                                        <tr class="table-data-row">
                                                                            <td><?= $flight_1->flightregion ?></td>
                                                                            <td><?= $flight_1->eet ?></td>
                                                                            <td class="no-border"><?= $flight_1->entry ?></td>
                                                                            <td><?= $flight_1->exit ?></td>
                                                                            <td><?= $flight_1->distanceb ?></td>
                                                                        </tr>
                                                                    <?php endforeach ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <?php if (!empty($res->flight_2)) { ?>

                                                            <div class="col-xs-6">
                                                                <table class="flight-information-region show-borders text-centered">
                                                                    <thead>
                                                                        <tr class="table-header-row">
                                                                            <th title="Flight information region" class="text-left">Flight Information Region</th>
                                                                            <th title="Estimated entry time">EET</th>
                                                                            <th title="Entry">ENTRY</th>
                                                                            <th title="Exit">EXIT</th>
                                                                            <th title="Distance">DISTANCE</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php foreach ($res->flight_2 as $key => $flight_2): ?>
                                                                            <tr class="table-data-row">
                                                                                <td><?= $flight_2->flightregion ?></td>
                                                                                <td><?= $flight_2->eet ?></td>
                                                                                <td class="no-border"><?= $flight_2->entry ?></td>
                                                                                <td><?= $flight_2->exit ?></td>
                                                                                <td><?= $flight_2->distanceb ?></td>
                                                                            </tr>
                                                                        <?php endforeach ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        <?php } ?>
                                                    </section>
                            <!--                            <section class="section row flight-information-region mt-20">
                                                            <div class="col-xs-6">
                                                                <table class="flight-information-region show-borders text-centered">
                                                                    <thead>
                                                                        <tr class="table-header-row">
                                                                            <th title="Flight information region" class="text-left border-bottom">Flight Information Region</th>
                                                                            <th title="Estimated entry time" class="border-bottom">EET</th>
                                                                            <th title="Entry" class="border-bottom">ENTRY</th>
                                                                            <th title="Exit" class="border-bottom">EXIT</th>
                                                                            <th title="Distance" class="border-bottom">DISTANCE</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <tr class="table-data-row">
                                                                            <td class="border-bottom"><?= $res->flightregion ?></td>
                                                                            <td class="border-bottom"><?= $res->eet ?></td>
                                                                            <td class="border-bottom"><?= $res->entry ?></td>
                                                                            <td class="border-bottom"><?= $res->exit ?></td>
                                                                            <td class="border-bottom"><?= $res->distanceb ?></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </section>-->

                                                    <table class="airport-frequencies text-centered mt-20">
                                                        <thead>
                                                            <tr class="table-header-row">
                                                                <th></th>
                                                                <th title="Airport" class="border-bottom">Airport</th>
                                                                <th title="Estimated time of arrival" class="border-bottom">ETA</th>
                                                                <th title="Weather" class="border-bottom">WX</th>
                                                                <th title="Tower / Common traffic advisory frequency" class="border-bottom">TWR/CTAF</th>
                                                                <th title="Clearance" class="border-bottom">CLR</th>
                                                                <th title="Ground" class="border-bottom">GND</th>
                                                                <th title="Elevation" class="border-right border-bottom">ELEV</th>
                                                                <th title="Longest Runway" colspan="2" class="text-centered border-bottom">LONGEST RWY</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php foreach ($res->airports as $key => $airport): ?>
                                                                <tr class="table-data-row">
                                                                    <td class="bold border-bottom" title="DEP"><?= $airport->fila1 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila2 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila3 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila4 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila5 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila6 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila7 ?></td>
                                                                    <td class="border-right border-bottom"><?= $airport->fila8 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila9 ?></td>
                                                                    <td class="border-bottom"><?= $airport->fila10 ?></td>
                                                                </tr>
                                                            <?php endforeach ?>
                                                        </tbody>
                                                    </table>
                                                    <div class="hr"></div>
                                                    </div>
                                                    </div>
                                                    </body>
                                                    </html>

