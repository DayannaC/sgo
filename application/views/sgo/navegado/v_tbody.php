<?php foreach ($res as $key => $val): ?>
    <tr>
        <td><?= $val->id ?> </td>
        <td><?= $val->titulo ?> <br> <?= $val->textofecha ?></td>
        <td><?= $val->ete ?> <br> <?= $val->distance ?></td>
        <td><?= $val->avgwind ?></td>
        <td><?= $val->etd ?> <br> <?= $val->eta ?></td>
        <td>
            <a href="<?= base_url('sgo/ver/' . $val->id . '?titulo=' . $val->titulo) ?> " class="btn btn-secondary" target="_blank"><i class="fa fa-eye"></i></a>
        </td>
        <?php
        $rpta = array_search('ver_pdf', $permisos);
        if ($rpta) {
            ?>
            <td>
                <a href="<?= base_url('sgo/GenerarPDF/' . $val->id . '?titulo=' . $val->titulo) ?> "  class="btn btn-outline-danger" target="_blank"><i class="fa fa-file-pdf"></i></a>
            </td>
            <?php
        }
        $rpta2 = array_search('aceptar_registro', $permisos);
        if ($rpta2) {
            ?>
            <td>
                <button onclick="aceptar_registro(<?= $val->id ?>)" class="btn btn-dark " target="_blank" <?= ($val->aceptar_registro == 'S') ? 'disabled' : '' ?>><i class="fas fa-file-signature"></i></button>
            </td>
            <?php
        }
        $rpta3 = array_search('editar', $permisos);
        if ($rpta3) {
            ?>
            <td>
                <a href="<?= base_url('sgo/editar/' . $val->id . '?titulo=' . $val->titulo) ?>" class="btn btn-primary <?= ($val->aceptar_registro == 'N' || $val->cerrar_registro == 'S' ) ? 'disabled' : '' ?>"><i class="fa fa-edit"></i></a>
            </td>
            <?php
        }
        $rpta4 = array_search('cerrar_registro', $permisos);
        if ($rpta4) {
            ?>
            <td>
                <button onclick="cerrar_registro(<?= $val->id ?>)" class="btn btn-danger" target="_blank"  <?= ($val->aceptar_registro == 'N' || $val->cerrar_registro == 'S') ? 'disabled' : '' ?>><i class="fas fa-file-signature"></i></button>
            </td>
            <?php
        }
        $rpta5 = array_search('eliminar', $permisos);
        if ($rpta5) {
            ?>
            <td>
                <a onclick="elminarRegistro(<?= $val->id ?>)" class="btn" style="background-color: #c90e14;color:white"><i class="fa fa-trash"></i></a>
            </td>
        <?php } ?>
    </tr>
<?php endforeach ?>

