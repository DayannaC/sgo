<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="viewport" content="width=960, initial-scale=1">
                <title>ForeFlight International Navlog</title>
                <script>
                    function setTheme(theme) {
                        var body = document.getElementsByTagName('body');
                        body[0].className = theme;
                        return 'Navlog theme set to ' + theme;
                    }
                </script>
                <?= link_tag('css/sgo/ver.css') ?>
                </head>
                <body>
                    <div id="wrapper">
                        <div class="container-fluid">
                            <section class="row section navlog-header">
                                <div class="col-xs-6 flight-summary">
                                    <strong>
                                        <?= $res->titulo ?>
                                    </strong>
                                    <div class="perf-summary">
                                        <?= $res->subtitulo ?>
                                    </div>
                                </div>
                                <div class="col-xs-6 row text-right date-created">
                                    <?= $res->textofecha ?>
                                </div>
                            </section>

                            <div class="hr"></div>

                            <section class="row section performance-summary">
                                <table>
                                    <tbody>
                                        <tr class="no-border">
                                            <td class="performance-metric ete">
                                                <strong>ETE</strong>
                                                <span><?= $res->ete ?></span>
                                            </td>
                                            <td class="performance-metric distance">
                                                <strong>Distance</strong>
                                                <span><?= $res->distance ?></span>
                                            </td>
                                            <td class="performance-metric avg-wind">
                                                <strong>Avg Wind</strong>
                                                <span class="no-wrap"><?= $res->avgwind ?></span>
                                            </td>
                                            <td class="performance-metric etd">
                                                <strong>ETD</strong>
                                                <span><?= $res->etd ?></span>
                                            </td>
                                            <td class="performance-metric eta">
                                                <strong>ETA</strong>
                                                <span><?= $res->eta ?></span>
                                            </td>
                                            <td class="performance-metric avg-tas">
                                                <strong>Avg TAS</strong>
                                                <span><?= $res->avgtas ?></span>
                                            </td>
                                            <td class="performance-metric altitude">
                                                <strong>Altitude</strong>
                                                <span><?= $res->altitude ?></span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </section>

                            <div class="hr"></div>

                            <section class="row section route">
                                <div class="col-xs-12">
                                    <strong>Route</strong>
                                    <div>
                                        <?= $res->route ?>
                                    </div>
                                </div>
                            </section>

                            <section class="section row fuel-weight-tables mt-10">
                                <div class="col-xs-5">
                                    <table class="fuel">
                                        <thead>
                                            <tr>
                                                <th title="Fuel">Fuel</th>
                                                <th title="fuelUnit" class="text-right">(lbs)</th>
                                                <th title="Time" class="text-right bold">Time</th>
                                                <th title="Time" class="text-right bold">Actuals</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->fuel as $key => $fuel): ?>
                                                <tr>
                                                    <td><?= $fuel->fuel ?></td>
                                                    <td class="text-right"><?= $fuel->lbs ?></td>
                                                    <td class="text-right bold"><?= $fuel->time ?></td>
                                                    <td><?= $fuel->actuals ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-xs-3">
                                    <table class="weights">
                                        <thead>
                                            <tr>
                                                <th title="Weights in pounds">Weights (lbs)</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->weight as $key => $weight): ?>
                                                <tr>
                                                    <td><?= $weight->weight ?></td>
                                                    <td class="text-right bold"><?= $weight->numero ?></td>
                                                </tr>

                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>

                                <div class="col-xs-4">
                                    <table class="info">
                                        <thead>
                                            <tr>
                                                <th title="Info" colspan="2">Info</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->info as $key => $info): ?>
                                                <tr>
                                                    <td><?= $info->info ?></td>
                                                    <td class="bold text-right"><?= $info->item ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>

                            <section class="row section tables-wrapper mt-10">
                                <div class="col-xs-6">
                                    <table class="alt-checks">
                                        <thead>
                                            <tr>
                                                <th title="Alternate Checks">Alt/Rvsm checks</th>
                                                <th title="Alternate 1">Alt #1</th>
                                                <th title="Alternate 2">Alt #2</th>
                                                <th title="Stand-by">Stand-by</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="4" class="tall">RAMP</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" class="tall" class="form-control" rows="4" contenteditable=""><?= $res->ramp ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-xs-6">
                                    <table class="notes-table">
                                        <thead>
                                            <tr class="notesHeaderRow">
                                                <th title="Notes" colspan="3">Notes</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->notes as $key => $note): ?>
                                                <tr>
                                                    <td class="tall"><?= $note->fila1 ?> <?= $note->valor1 ?></td>
                                                    <td class="tall"><?= $note->fila2 ?> <?= $note->valor2 ?></td>
                                                    <td class="tall bold"><?= $note->fila3 ?> <?= $note->valor3 ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </section>

                            <section class="section row clearance notes mt-10">
                                <div class="dont-break-container col-xs-12 mb-10">
                                    <div class="writing-line">
                                        <strong>Departure ATIS</strong>
                                    </div>
                                    <div class="writing-line"><?= $res->departureatis ?></div>
                                    <div class="writing-line"></div>
                                </div>
                            </section>

                            <section class="section row clearance notes mt-10">
                                <div class="col-xs-12 dont-break-container">
                                    <div class="writing-line">
                                        <strong>Clearance</strong>
                                    </div>
                                    <div class="writing-line"><?= $res->clearance ?></div>
                                    <div class="writing-line"></div>
                                </div>
                            </section>

                            <table class="way-point mt-20 text-centered show-borders">
                                <thead class="multi-row">
                                    <tr style="display: none;">
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    <tr class="waypoint-header-row1">
                                        <th colspan="2"></th>
                                        <th title="Airway">AWY</th>
                                        <th title="Wind">WIND</th>
                                        <th title="Magnetic heading">MAG</th>
                                        <th></th>
                                        <th title="Distance">DIST</th>
                                        <th title="Fuel Pounds" colspan="3">FUEL lbs</th>
                                        <th title="Times" colspan="3">TIMES</th>
                                        <th title="Remarks" colspan="3" class="remarks">REMARKS</th>
                                    </tr>
                                    <tr class="waypoint-header-row2">
                                        <th title="Waypoint" colspan="2" class="text-left">Waypoint</th>
                                        <th title="Altitude">ALT</th>
                                        <th title="Wind direction / Wind speed">DIR/SPD</th>
                                        <th title="Heading">HDG</th>
                                        <th title="True airspeed">TAS</th>
                                        <th title="Remaining distance">LEG</th>
                                        <th title="Remaining fuel">REM</th>
                                        <th title="Fuel used">USED</th>
                                        <th title="Fuel flow">FLOW</th>
                                        <th title="Time to fly through leg">LEG</th>
                                        <th title="Estimated time enroute">ETE</th>
                                        <th title="Estimated time of arrival">ETA</th>
                                        <th colspan="3"></th>
                                    </tr>
                                    <tr class="waypoint-header-row3">
                                        <th title="Coordinates" colspan="2" class="text-left normal">Coordinates</th>
                                        <th title="Minimum safe altitude">MSA</th>
                                        <th title="ISA deviation">ISA</th>
                                        <th title="Course">CRS</th>
                                        <th title="Ground airspeed">GS</th>
                                        <th title="Distance remaining">REM</th>
                                        <th></th>
                                        <th title="Actual fuel used">ACTL</th>
                                        <th></th>
                                        <th title="Time remaining">REM</th>
                                        <th title="Actual time enroute">ATE</th>
                                        <th title="Actual time of arrival" class="ata">ATA</th>
                                        <th colspan="3"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($res->rutas as $key => $ruta): ?>
                                        <?php if ($ruta->columna == 1): ?>
                                            <tr class="table-data-row">
                                                <td colspan="2"><?= $ruta->fila1 ?></td>
                                                <td><?= $ruta->fila2 ?></td>
                                                <td><?= $ruta->fila3 ?></td>
                                                <td><?= $ruta->fila4 ?></td>
                                                <td><?= $ruta->fila5 ?></td>
                                                <td><?= $ruta->fila6 ?></td>
                                                <td><?= $ruta->fila7 ?></td>
                                                <td><?= $ruta->fila8 ?></td>
                                                <td><?= $ruta->fila9 ?></td>
                                                <td><?= $ruta->fila10 ?></td>
                                                <td><?= $ruta->fila11 ?></td>
                                                <td><?= $ruta->fila12 ?></td>
                                                <td colspan="3"><?= $ruta->fila13 ?></td>
                                            </tr>
                                        <?php else: ?>
                                            <tr class="sub-header">
                                                <td title="Alternate 1 Route" class="text-left" id="alternateRoute" colspan="19"><?= $ruta->fila1 ?></td>
                                            </tr>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>

                            <section class="section row clearance notes mt-10">
                                <div class="dont-break-container col-xs-12 mb-10">
                                    <strong>Arrival ATIS</strong>
                                </div>
                                <div class="writing-line"><?= $res->arrivalatis ?></div>
                                <div class="writing-line"></div>
                        </div>
                        </section>

                        <table class="alt-advisor show-borders mt-20">
                            <thead class="multi-row">
                                <tr class="alt-advisor-header row1">
                                    <th colspan="2"></th>
                                    <th title="Performance profile" class="text-centered" colspan="2">Long Range Cruise (PLAN)</th>
                                    <th title="Performance profile" class="text-centered" colspan="2">Mach 0.82</th>
                                    <th title="Performance profile" class="text-centered" colspan="2">Mach 0.80</th>
                                </tr>
                                <tr class="alt-advisor-header row2">
                                    <th title="Altitude">Altitude</th>
                                    <th title="Average wind" class="border-right text-centered">Avg wind</th>
                                    <th title="Time" class="text-left normal">TIME</th>
                                    <th title="Fuel" class="text-right border-right normal">FUEL</th>
                                    <th title="Time" class="text-left normal">TIME</th>
                                    <th title="Fuel" class="text-right border-right normal">FUEL</th>
                                    <th title="Time" class="text-left normal">TIME</th>
                                    <th title="Fuel" class="text-right normal">FUEL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($res->latitudes as $key => $latitude): ?>
                                    <tr class="alt-advisor-data">
                                        <td><?= $latitude->fila1 ?></td>
                                        <td class="text-centered"><?= $latitude->fila2 ?></td>
                                        <td class="no-border"><?= $latitude->fila3 ?></td>
                                        <td class="text-right"><?= $latitude->fila4 ?></td>
                                        <td class="no-border"><?= $latitude->fila5 ?></td>
                                        <td class="text-right"><?= $latitude->fila6 ?></td>
                                        <td class="no-border"><?= $latitude->fila7 ?></td>
                                        <td class="text-right"><?= $latitude->fila8 ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>

                        <section class="section row flight-information-region mt-20">
                            <div class="col-xs-6">
                                <table class="flight-information-region show-borders text-centered">
                                    <thead>
                                        <tr class="table-header-row">
                                            <th title="Flight information region" class="text-left">Flight Information Region</th>
                                            <th title="Estimated entry time">EET</th>
                                            <th title="Entry">ENTRY</th>
                                            <th title="Exit">EXIT</th>
                                            <th title="Distance">DISTANCE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($res->flight_1 as $key => $flight_1): ?>
                                            <tr class="table-data-row">
                                                <td><?= $flight_1->flightregion ?></td>
                                                <td><?= $flight_1->eet ?></td>
                                                <td class="no-border"><?= $flight_1->entry ?></td>
                                                <td><?= $flight_1->exit ?></td>
                                                <td><?= $flight_1->distanceb ?></td>
                                            </tr>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                            <?php if (!empty($res->flight_2)) { ?>

                                <div class="col-xs-6">
                                    <table class="flight-information-region show-borders text-centered">
                                        <thead>
                                            <tr class="table-header-row">
                                                <th title="Flight information region" class="text-left">Flight Information Region</th>
                                                <th title="Estimated entry time">EET</th>
                                                <th title="Entry">ENTRY</th>
                                                <th title="Exit">EXIT</th>
                                                <th title="Distance">DISTANCE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($res->flight_2 as $key => $flight_2): ?>
                                                <tr class="table-data-row">
                                                    <td><?= $flight_2->flightregion ?></td>
                                                    <td><?= $flight_2->eet ?></td>
                                                    <td class="no-border"><?= $flight_2->entry ?></td>
                                                    <td><?= $flight_2->exit ?></td>
                                                    <td><?= $flight_2->distanceb ?></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </section>

                        <table class="airport-frequencies text-centered mt-20">
                            <thead>
                                <tr class="table-header-row">
                                    <th></th>
                                    <th title="Airport">Airport</th>
                                    <th title="Estimated time of arrival">ETA</th>
                                    <th title="Weather">WX</th>
                                    <th title="Tower / Common traffic advisory frequency">TWR/CTAF</th>
                                    <th title="Clearance">CLR</th>
                                    <th title="Ground">GND</th>
                                    <th title="Elevation" class="border-right">ELEV</th>
                                    <th title="Longest Runway" colspan="2" class="text-centered">LONGEST RWY</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($res->airports as $key => $airport): ?>
                                    <tr class="table-data-row">
                                        <td class="bold" title="DEP"><?= $airport->fila1 ?></td>
                                        <td><?= $airport->fila2 ?></td>
                                        <td><?= $airport->fila3 ?></td>
                                        <td><?= $airport->fila4 ?></td>
                                        <td><?= $airport->fila5 ?></td>
                                        <td><?= $airport->fila6 ?></td>
                                        <td><?= $airport->fila7 ?></td>
                                        <td class="border-right"><?= $airport->fila8 ?></td>
                                        <td><?= $airport->fila9 ?></td>
                                        <td><?= $airport->fila10 ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                        <div class="hr"></div>
                    </div>
                    </div>
                </body>
                </html>

