<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-1">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Plan de Vuelo Navegado</a></li>
        </ol>
    </div>
    <div class="row">
        <div class="col-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">PLAN DE VUELO NAVEGADO</h1>
                    <hr class="sidebar-divider">
                </div>
                <div class="container-fluid mb-1">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <?php
//                                var_dump($permisos);
//                                $cant_permisos = count($permisos) - 1;

                                $rpta0 = array_search('subir', $permisos);
                                if ($rpta0) {
                                    ?>
                                    <input type="file" name="file" id="file" accept=".html">
                                    <div id="salida"></div>
                                    <pre class="w-100" id="json"></pre>
                                <?php } ?>
                            </div>
                            <div id="wrapper">
                                <div class="table-responsive">
                                    <table class="table table-bordered" id="tbody"  style="width:100%">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>ETE / Distance</th>
                                                <th>Avg Wind</th>
                                                <th>ETD / ETA</th>
                                                <th colspan="5">OPCIONES</th>
                                            </tr>
                                            <tr hidden>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $this->load->view('sgo/navegado/v_tbody'); ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






