<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="">Tabla de pesos y combustibles</a></li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 shadow-lg p-3 mb-5 bg-white rounded">
<!--            <table id="lista_formularios" class="table table-bordered display nowrap" style="width:100%">
            <thead>
                <tr id="cabecera">
                    <th hidden>idUsuario</th>
                    <th scope="col">Codigo</th>
            <?php foreach ($subelementos as $subelemento) { ?>
                                                                                                                                                <th scope="col"><?= $subelemento->nombre ?></th>
            <?php } ?>
                    <th scope="col">Estado</th>
                </tr>
            </thead>
            <tbody>
                <tr id="">

                </tr>
            </tbody>
        </table>-->
            <div class="table-responsive">
                <table id="tabla" class="table table-striped table-bordered" style="width: 800px;">
                    <caption id="menuAcciones">
                        <button type="button" class="btnAgregar">+</button>
                    </caption>
                    <caption></caption>
                    <thead>
                        <tr>
                            <th scope="col" class="text-center">Fecha</th>
                            <th scope="col" class="text-center">Codigo</th>
                            <?php foreach ($subelementos as $subelemento) { ?>
                                <th scope="col" colspan="2" class="text-center"><?= $subelemento->nombre ?></th>
                            <?php } ?>
                            <th class="text-center">Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
    <!--            <table id="tabla">
                    <caption id="menuAcciones">
                        <button type="button" class="btnAgregar">+</button>
                    </caption>
                    <caption></caption>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Select element</th>
                            <th>Radio Elements</th>
                            <th>Checkbox Elements</th>
                            <th>Datalist</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>-->
        </div>
    </div>
</div>
