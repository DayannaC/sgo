<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Weight and balance - load sheet</a></li>
        </ol>
    </div>
    <div class="row ">
        <div class="col-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">WEIGHT AND BALANCE - LOAD SHEET</h1>
                    <hr class="sidebar-divider">
                </div>
                <div class="box-body">
                    <div class="container-fluid border border-dark my-1">
                        <div class="row">
                            <div class="col-lg-2">
                                <label for="">Flight N°</label>
                            </div>
                            <div class="col-lg-2">
                                <label for="">A/C Reg.</label>
                            </div>
                            <div class="col-lg-2">
                                <label for="">FROM  TO:</label>
                            </div>
                            <div class="col-lg-2">
                                <label for="">Version:</label>
                            </div>
                            <div class="col-lg-2">
                                <label for="">Crew:</label>
                            </div>
                            <div class="col-lg-2">
                                <label for="">Date:</label>
                            </div>
                        </div> 
                        <div class="row mb-2">
                            <div class="col-lg-2 border" contenteditable="true">
                            </div>
                            <div class="col-lg-2 border" contenteditable="true">
                            </div>
                            <div class="col-lg-2 border" contenteditable="true">
                            </div>
                            <div class="col-lg-2 border" contenteditable="true">
                            </div>
                            <div class="col-lg-2 border" contenteditable="true">
                            </div>
                            <div class="col-lg-2 border" contenteditable="true">
                            </div>
                        </div> 
                    </div>
                    <div class="container-fluid border border-dark my-1">
                        <div class="row">
                            <div class="col-lg-2 border">
                                <label>Operating Weigth Empty</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 border">
                                <label>MAXIMUM WEIGHTS FOR</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border">
                                        <label>ZERO FUELL</label>
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border">
                                        <label>LANDING</label>
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 border">
                                <label>Adjustements</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 border">
                                <label>Take Off Fuel +</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border">
                                        <label>ZERO FUELL</label>
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">

                            </div>
                            <div class="col-lg-2 border">
                                <label>LANDING</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 border">
                                <label>Corrected</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 border">
                                <label>ALLOWED WEIGTH FOR TAKE OFF</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 border">
                                <label>Take Off Fuel </label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 border">
                                <label>OPERATING WEIGHT -</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 border">
                                <label>OPERATING WEIGHT</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 border">
                                <label>ALLOWED TRAFIC LOAD</label>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2">
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>  
                                </div>
                                <div class="row">
                                    <div class="col-lg border" contenteditable="true">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark my-1">
                        <div class="row">
                            <div class="col-lg-6">
                                <label>DASH 8 SERIE 400 MODEL 402</label>
                            </div>
                            <div class="col-lg-6 border-left">
                                <label>76 PASSENGER VERSION </label>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark">
                        <div class="row">
                            <div class="col-lg-6" rowspan="9">
                                <img src="<?= base_url() ?>img/sgo/DASH 8 SERIE 400 MODEL 402.JPG" width="500">
                            </div>
                            <div class="col-lg-6 border border-dark">
                                <div class="container border border-dark my-1">
                                    <div class="row mt-1">
                                        <div class="col-lg-8 border-right">
                                            <label>ALLOWED TRAFIC LOAD</label>
                                        </div>
                                        <div class="col-lg-4 border" contenteditable="true">
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="col-lg-8 border-right">
                                            <label>TOTAL TRAFIC LOAD - </label>
                                        </div>
                                        <div class="col-lg-4 border" contenteditable="true">
                                        </div>
                                        <div class="w-100"></div>
                                        <div class="col-lg-8 border-right">
                                            <label>UNDER LOAD(BEFORE LMC)</label>
                                        </div>
                                        <div class="col-lg-4 border" contenteditable="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="row mt-1">
                                    <div class="col-lg-12">
                                        <label class="">Check LMC TOTAL with UNDER LOAD</label>
                                    </div>
                                </div>
                                <div class="container-fluid border border-dark my-1">
                                    <div class="row mt-1">
                                        <div class="col-lg-12 border">
                                            <label>LAST MINUTE CHANGE(LMC)</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 border">
                                            <label>SPECIFICATION</label>
                                        </div>
                                        <div class="col-lg-2 border">
                                            <label>COMP</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>+/-</label>
                                        </div>
                                        <div class="col-lg-5 border">
                                            <label>WEIGHT</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 border">
                                            <label>LMC TOTAL</label>
                                        </div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark my-1">
                        <div class="row">
                            <div class="col-lg-6">
                                <!--                                <div class="container-fluid">-->
                                <div class="row my-1">
                                    <div class="col-lg-3">

                                    </div>
                                    <div class="col-lg-1">

                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">MAX</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">NO PAX</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">WEIGHT Lbs</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">INDEX</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">OWE</label>
                                    </div>
                                    <div class="col-lg-1">

                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">OBSERVER</label>
                                    </div>
                                    <div class="col-lg-1">

                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">WARDROBE</label>
                                    </div>
                                    <div class="col-lg-1">

                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">160 Lbs</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <label>AD</label>
                                            </div>
                                            <div class="col-lg-4 pl-2">
                                                <label>CHD</label>
                                            </div>
                                            <div class="col-lg-4 pl-1">
                                                <label>INF</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">1A</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">2 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">0</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">1</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">8 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">0</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                 <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">2</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">12 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">4</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                 <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">3</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">12 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">10</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                 <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">4</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">12 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">10</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                 <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">5</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">12 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">10</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                 <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">6</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">12 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">0</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">PAX COMPT</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class="">7</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">6 Pax</label>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="row">
                                            <div class="col-lg-4 border">
                                                <label class="">0</label>
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                            <div class="col-lg-4 border" contenteditable="true">
                                                <!--<input type="text" name="" class="form-control form-control-user" required="" />-->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">BAGS COMPT</label>
                                    </div>
                                    <div class="col-lg-1 px-1">
                                        <label class="">FWD</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">510 Lbs</label>
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">BAGS COMPT</label>
                                    </div>
                                    <div class="col-lg-1 px-1">
                                        <label class="">AFT1</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">2600 Lbs</label>
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">BAGS COMPT</label>
                                    </div>
                                    <div class="col-lg-1 px-1">
                                        <label class="">AFT2</label>
                                    </div>
                                    <div class="col-lg-2 border">
                                        <label class="">1000 Lbs</label>
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">TOTAL TRAFIC LOAD</label>
                                    </div>
                                    <div class="col-lg-1">
                                        <label class=""></label>
                                    </div>
                                    <div class="col-lg-2 ">
                                        <label class=""></label>
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-2">
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">FUEL ON BOARD</label>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-2">
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-1 ">
                                    </div>
                                    <div class="col-lg-2">
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">TAXI FUEL</label>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-2">
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-1 px-1">
                                        <label class="">LMC</label>
                                    </div>
                                    <div class="col-lg-3 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-1">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <label class="">TRIP FUEL</label>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-2 ">
                                    </div>
                                    <div class="col-lg-2">

                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-1 px-1">
                                        <label class="">LMC</label>
                                    </div>
                                    <div class="col-lg-3 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <div class="row mb-1">
                                    <div class="col-lg-3">
                                        <label class="">TOTAL N° of PAX</label>
                                    </div>
                                    <div class="col-lg-1 bordder">
                                        <label class="">34</label>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="row">
                                            <div class="col-lg-4 border" contenteditable="true">34</div>
                                            <div class="col-lg-4 border" contenteditable="true">0</div>
                                            <div class="col-lg-4 border" contenteditable="true">0</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-1">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                    <div class="col-lg-2 border" contenteditable="true">
                                    </div>
                                </div>
                                <!--</div>-->
                            </div>
                            <div class="col-lg-6 border-left border-dark">
                                <div class="container-fluid my-1">
                                    <div class="row border mt-1">
                                        <label>PASSENGER</label>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border">
                                            <label>No of PAX</label>
                                        </div>
                                        <div class="col-lg-2 border">
                                            <label>Weight Lbs</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 1A 0-2PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 1 0-8PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 2 0-12PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 3 0-12PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 4 0-12PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 5 0-12PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 6 0-12PAX</label>
                                        </div>
                                        <div class="col-lg-1 border">
                                            <label>Com 7 0-8PAX</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">1</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">2</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">3</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">4</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">5</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">6</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">7</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">7</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">8</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">9</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">10</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-2 border" contenteditable="true">11</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-lg-2 border" contenteditable="true">12</div>
                                        <div class="col-lg-2 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                        <div class="col-lg-1 border" contenteditable="true">
                                        </div>
                                    </div>
                                </div>
                                <div class="conteiner-fluid">
                                    <div class="row m-1">
                                        <div class="col-lg-6">
                                            <div class="mt-1">
                                                <div class="row">
                                                    <div class="col-lg-12 border">OBSERVER</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border">Weight Lbs</div>
                                                    <div class="col-lg-6 border">INDEX</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border" contenteditable="true">198</div>
                                                    <div class="col-lg-6 border" contenteditable="true">-8.7</div>
                                                </div>
                                            </div>
                                            <div class="mt-3">
                                                <div class="row">
                                                    <div class="col-lg-6 border">RWY</div>
                                                    <div class="col-lg-6 border" contenteditable="true">17</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border">OAT</div>
                                                    <div class="col-lg-6 border" contenteditable="true">30</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border">WIND</div>
                                                    <div class="col-lg-6 border" contenteditable="true">V/C</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="m-1">
                                                <div class="row">
                                                    <div class="col-lg-12 border">WARDROBE</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border">Weight Lbs</div>
                                                    <div class="col-lg-6 border">INDEX</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border" contenteditable="true">20</div>
                                                    <div class="col-lg-6 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border" contenteditable="true">40</div>
                                                    <div class="col-lg-6 border" contenteditable="true">-1.6</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border" contenteditable="true">100</div>
                                                    <div class="col-lg-6 border" contenteditable="true">-4.1</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border" contenteditable="true">140</div>
                                                    <div class="col-lg-6 border" contenteditable="true">-5.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 border" contenteditable="true">160</div>
                                                    <div class="col-lg-6 border" contenteditable="true">-6.6</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="m-0 p-0" style="">
                                                <div class="row">
                                                    <div class="col-lg-12 border">FUELL</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" >Weight Lbs</div>
                                                    <div class="col-lg-2 border">INDEX</div>
                                                    <div class="col-lg-2 border">Weight Lbs</div>
                                                    <div class="col-lg-2 border">INDEX</div>
                                                    <div class="col-lg-2 border">Weight Lbs</div>
                                                    <div class="col-lg-2 border">INDEX</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-2 border" contenteditable="true">20</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                    <div class="col-lg-2 border" contenteditable="true">-0.8</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container-fluid border border-dark my-1">
                        <div class="row">
                            <div class="col-lg-5">
                                <img src="<?= base_url() ?>img/sgo/limit.JPG" width="400">
                            </div>
                            <div class="col-lg-7 border-left">
                                <div class="ml-0" style="">
                                    <div class="row">
                                        <div class="col-lg-12 border">FUELL</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border p-0">Weight Lbs</div>
                                        <div class="col-lg border p-0">Index</div>
                                        <div class="col-lg border p-0">Weight Lbs</div>
                                        <div class="col-lg border p-0">Index</div>
                                        <div class="col-lg border p-0">Weight Lbs</div>
                                        <div class="col-lg border p-0">Index</div>
                                        <div class="col-lg border p-0">Weight Lbs</div>
                                        <div class="col-lg border p-0">Index</div>
                                        <div class="col-lg border p-0">Weight Lbs</div>
                                        <div class="col-lg border p-0">Index</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border bg-secondary"></div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                        <div class="col-lg border" contenteditable="true">20</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>