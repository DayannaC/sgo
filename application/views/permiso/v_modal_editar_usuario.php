<div class="modal" id="modal_editar_usuario">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder" id="nomElemento">EDITAR USUARIO</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cerrarModalPermiso()">
                    <span aria-hidden="true"></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="">Id:</label>
                                <input type="text" name="adm_id" id="adm_id" class="form-control form-control-user" maxlength="100" required=""  required="" />
                            </div>
                            <div class="form-group">
                                <label for="">Nombres:</label>
                                <input type="text" name="nombres" id="adm_nombres" class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" required="" />
                            </div>
                            <div class="form-group">
                                <label for="">Apellido Paterno:</label>
                                <input type="text" name="ape_paterno" id="adm_ape_paterno"  class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" required=""/>
                            </div>
                            <div class="form-group">
                                <label for="">Apellido Materno:</label>
                                <input type="text" name="ape_materno" id="adm_ape_materno" class="form-control form-control-user" maxlength="100" onkeyup="convertirMayuscula(this)" />
                            </div>
                            <div class="form-group">
                                <label for="">Correo:</label>
                                <input type="email" name="correo" id="adm_correo" class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" required=""/>
                            </div>
                            <div class="form-group">
                                <label for="">Rol:</label>
                                <select class="form-control" name="rol" id="adm_rol" required="">
                                    <!--<option value="">SELECCIONAR</option>-->
                                    <?php foreach ($roles as $rol) { ?>
                                        <option value="<?= $rol->id_rol ?>"><?= $rol->rol ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">           
                        <button type="submit" onclick="EditarUsuario(this)" name="btn_editar_usuario" class="btn btn-primary btn-user btn-block">Enviar</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">           
                        <button type="button" id="doc_cerrar" class="btn btn-secondary" data-dismiss="modal" onclick="cerrarModalPermiso()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>