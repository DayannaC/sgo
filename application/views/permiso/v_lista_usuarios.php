<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800 font-weight-bold">Lista de Usuarios</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Lista de Usuario</a></li>
        </ol>
    </div>
    <div class="row ">
        <div class="col-sm-12 offset-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <table id="example" class="table table-bordered" style="width:100%">
                    <thead>
                        <tr id="cabecera" class="text-center">
                            <th hidden>idUsuario</th>
                            <th scope="col">Codigo</th>
                            <th scope="col">Nombres</th>
                            <th scope="col">Apellidos</th>
                            <th scope="col">Correo</th>                          
                            <th scope="col">Rol</th>
                            <th scope="col">Cambiar Clave</th>
                            <th scope="col">Editar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($datosUsuarios as $datoUsuario) {
                            ?>
                            <tr>
                                <td hidden><?= $datoUsuario->id_usuario ?></td>
                                <td class="font-weight-bold"><?= $datoUsuario->codigo ?></td>
                                <td><?= $datoUsuario->nombres ?></td>
                                <td><?= $datoUsuario->ape_paterno . ' ' . $datoUsuario->ape_materno ?></td>
                                <td><?= $datoUsuario->correo ?></td>
                                <td><?= ($datoUsuario->rol) ? $datoUsuario->rol : '' ?></td>
                                <td class="text-center"><button type="button" class="btn btn-primary" ><i class="fas fa-fw fa-lock"></i></button></td>
                                <td class="text-center"><button type="button" class="btn btn-primary" onclick="ObtenerDatosUsuario(this)" data-toggle="modal" data-target="#modal_editar_usuario"><i class="fas fa-fw fa-edit"></i></button></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('permiso/v_modal_editar_usuario'); ?>
