<div class="modal" id="modal_subelementos">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bolder" id="nomElemento"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cerrarModalPermiso()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="usuario">
                <input type="hidden" id="idElemento">
                <input type="hidden" id="nomElemento">
                <div class="row">
                    <div class="col-md-12">
                        <span id="nomSubelementos"></span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-12">           
                        <button type="button" id="doc_cerrar" class="btn btn-secondary" data-dismiss="modal" onclick="cerrarModalPermiso()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

