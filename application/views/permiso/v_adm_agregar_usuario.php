<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800"></h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Agregar Usuario</a></li>
        </ol>
    </div>
    <div class="row justify-content-center">
        <div class="col-sm-6 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <div class="box-header with-border">
                    <h1 class="h3 mb-0 text-gray-700 text-center font-weight-bold">Agregar Usuario</h1>
                    <hr class="sidebar-divider">
                </div>
                <?php echo form_open('Permiso/guardarUsuario', ['id' => 'form-usuario']); ?>
                <div class="box-body">
                    <div class="form-group">
                        <label for="">Nombres:</label>
                        <input type="text" name="nombres" class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" required="" />
                    </div>
                    <div class="form-group">
                        <label for="">Apellido Paterno:</label>
                        <input type="text" name="ape_paterno" class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" required=""/>
                    </div>
                    <div class="form-group">
                        <label for="">Apellido Materno:</label>
                        <input type="text" name="ape_materno" class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" />
                    </div>
                    <div class="form-group">
                        <label for="">Correo:</label>
                        <input type="email" name="correo" class="form-control form-control-user" maxlength="100" required="" onkeyup="convertirMayuscula(this)" required=""/>
                    </div>
<!--                    <div class="form-group">
                        <label for="">Rol:</label>
                        <select class="form-control" name="rol" required="">
                            <?php foreach ($roles as $rol){ ?>
                            <option value="<?= $rol->id_rol?>"><?= $rol->rol?></option>
                            <?php } ?>
                        </select>
                    </div>-->
                    <div class="box-footer">
                        <button type="submit" id="agregar_usuario" name="agregar_usuario" class="btn btn-primary btn-user btn-block">Enviar</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>