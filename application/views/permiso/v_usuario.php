<div class="container-fluid" id="container-wrapper">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Permisos</h1>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="<?= base_url('welcome') ?>">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="">Agregar Usuario</a></li>
        </ol>
    </div>
    <div class="row ">
        <div class="col-sm-12 offset-sm-12 shadow-lg p-3 mb-5 bg-white rounded">
            <div class="box box-warning">
                <input type="hidden" id="cantUsuarios" value="<?= count($usuarios) ?>">
                <input type="hidden" id="cantElementos" value="<?= count($codigoElementos) ?>">
                <table class="table table-bordered" id="tablaPermisos">
                    <thead>
                        <tr id="cabecera">
                            <th hidden>idUsuario</th>
                            <th scope="col">Codigo</th>
                            <?php foreach ($elementos as $elemento) { ?>
                                <th scope="col"><?= $elemento->nombre ?></th>
                            <?php } ?>
                            <th scope="col">Estado</th>
                        </tr>
                    </thead>
                    <tbody>                            
                        <?php
                        foreach ($usuarios as $usuario) {
                            ?>
                            <tr id="">
                                <th hidden><?= $usuario->idUsuario ?></th>
                                <th scope = "row"><?= $usuario->codigo ?></th>
                                <?php
                                foreach ($codigoElementos as $codigoElemento) {
                                    ?>
                                    <td id="checkbox<?= '-' . $codigoElemento->idElemento . '-' . $usuario->idUsuario ?>">
                                        <div class="custom-control custom-checkbox">
                                            <input name="<?= $codigoElemento->idElemento . '-' . $usuario->codigo ?>" type="checkbox" readonly="readonly" class="custom-control-input" id="<?= $codigoElemento->codigo, '-', $usuario->codigo ?>" value="<?= $codigoElemento->nombre, '-', $codigoElemento->idElemento, '-', $usuario->codigo ?>" onclick="obtenerSubelementos(this.value)" <?= ( (strpos($usuario->elementos, $codigoElemento->idElemento)) === false) ? '' : 'checked' ?>  <?= ($usuario->estadoUsuario == 'I' || $usuario->estadoUsuario == '') ? 'disabled' : '' ?>>
                                            <label class="custom-control-label" for="<?= $codigoElemento->codigo, '-', $usuario->codigo ?>" <?= ($usuario->estadoUsuario == 'I' || $usuario->estadoUsuario == '') ? 'style="cursor: not-allowed"' : 'style="cursor:pointer"' ?>></label>
                                        </div>
                                    </td>
                                    <?php
                                }
                                ?>
                                <td id="checkEstado">
                                    <div class="custom-control custom-switch text-center">
                                        <input type="checkbox" class="custom-control-input" id="<?= $usuario->codigo, '-', $usuario->idUsuario ?>" value="<?= $usuario->codigo, '-', $usuario->idUsuario ?>" onclick="cambiarEstadoUsuario(this.value)" <?php
                                        if ($usuario->idUsuario) {
                                            if ($usuario->estadoUsuario == 'A') {
                                                echo 'checked';
                                            } else {
                                                echo '';
                                            }
                                        } else {
                                            echo '';
                                        }
                                        ?>>
                                        <label class="custom-control-label" for="<?= $usuario->codigo, '-', $usuario->idUsuario ?>"></label>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('permiso/v_modal_subelementos'); ?>
