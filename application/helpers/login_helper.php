<?php

function setUserData($datosUsuario) {
    $CI = &get_instance();
    $CI->session->set_userdata(['usuario' => $datosUsuario]);
}

if (!function_exists('auth')) {

    function auth() {
        $CI = &get_instance();
        return $CI->session->userdata('usuario');
    }

}

function is_logged_in() {
    if (null == auth()) {
        header('Location: ' . base_url('inicio'));
    }
}
