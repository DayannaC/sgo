$(document).ready(function () {
    $('#tablaPermisos').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla.",
            "info": "Del _START_ al _END_ de _TOTAL_ ",
            "infoEmpty": "Mostrando 0 registros de un total de 0.",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "infoPostFix": "registros",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar ",
//            "searchPlaceholder": "Dato para buscar",
            "zeroRecords": "No se han encontrado coincidencias.",
            "paginate": {
                "first": "Primera",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
//        ,
//        "aoColumnDefs": [
//            {"bSortable": false, "aTargets": [2, 3, 4, 5, 6]}
//        ]
    });
});

$(':checkbox[readonly=readonly]').click(function () {
    return false;
});

function obtenerSubelementos(valores) {
    var usuario = "";
    var valor = valores.split('-');
    var nomElemento = valor[0];
    var idElemento = valor[1];
    var usuario = valor[2];
    console.log(usuario);
    $('#nomElemento').text(nomElemento);
    $('#usuario').val(usuario);
    $('#idElemento').val(idElemento);
    var data = 'idElemento=' + idElemento + '&usuario=' + usuario;
    var ruta = URLs + 'Permiso/obtenerSubelementos';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        success: function (rpta) {
            var objeto = JSON.parse(rpta);
            objeto.forEach(value => {
                var nomSub = '\
                        <tr>\n\
                            <td scope="col">' + value['nomSubelemento'] + ' </td>\n\
                            <td>\n\
                                <div class="custom-control custom-checkbox">\n\
                                <input type="checkbox" class="custom-control-input ' + idElemento + '" id="' + idElemento + '-' + value['idSubelemento'] + '-' + usuario + '" value="' + idElemento + '-' + value['idSubelemento'] + '-' + usuario + '" onclick="darPermisos(' + value['idSubelemento'] + ')">\n\
                                <label class="custom-control-label" for="' + idElemento + '-' + value['idSubelemento'] + '-' + usuario + '" ></label>\n\
                                </div>\n\
                        </td></tr>';
                marcarSubelementos(usuario, idElemento, value['idSubelemento']);
                $('#nomSubelementos').append(nomSub);
            })
        }
    });
}

function  marcarSubelementos(usuario, idElemento, idSubelemento) {
    var data = 'usuario=' + usuario + '&idElemento=' + idElemento + '&idSubelemento=' + idSubelemento + '&accion=marcar_checkbox';
    var ruta = URLs + 'Permiso/darPermisos';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        success: function (rpta) {
            if (rpta == 'A') {
                $('#' + idElemento + '-' + idSubelemento + '-' + usuario).prop('checked', true);
            }
            $('#modal_subelementos').modal({
                show: true,
                backdrop: 'static',
                keyboard: false
            })
        }
    });
}

function cerrarModalPermiso() {
//    debugger
    var idElemento = $('#idElemento').val();
    var usuario = $('#usuario').val();
    var contador = 0;
    $('input[type=checkbox].' + idElemento).each(function () {
        if ($(this).is(':checked'))
            contador++;
    });
    if (contador > 0) {
        $('input[name=' + idElemento + '-' + usuario + ']').prop('checked', true);
    } else {
        $('input[name=' + idElemento + '-' + usuario + ']').prop('checked', false);
    }
    actualizarPermisos();
    $('#nomSubelementos').text('');
}

function actualizarPermisos() {
    $('#sidebar').load('    #sidebar');
}

function cambiarEstadoUsuario(usuario) {
//    debugger
    var valor = usuario.split('-');
    var codUsuario = valor[0];
    var idUsuario = valor[1];
    var estado = '';
    var checkbox = document.getElementById(usuario);
    if (checkbox.checked == true) {
        estado = 'activar';
    } else {
        estado = 'desactivar';
    }
    var data = 'codUsuario=' + codUsuario + '&idUsuario=' + idUsuario + '&estado=' + estado;
    var ruta = URLs + 'Permiso/cambiarEstadoUsuario';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        success: function (rpta) {
            var cantElementos = $('#cantElementos').val();
            for (var i = 1; i <= cantElementos; i++) {
                var td = document.getElementById('checkbox-' + i + '-' + idUsuario);
                var idInput = td.getElementsByTagName("input")[0].id;
                if (estado == 'desactivar') {
                    $('#' + idInput).prop('disabled', true);
                } else {
                    $('#' + idInput).prop('disabled', false);
                }
            }
        }
    });
}

function darPermisos(idSubelemento) {
//    debugger;
    var usuario = $('#usuario').val();
    var idElemento = $('#idElemento').val();
    var data = 'idSubelemento=' + idSubelemento + '&idElemento=' + idElemento + '&usuario=' + usuario + '&accion=dar_permisos';
    var ruta = URLs + 'Permiso/darPermisos';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        success: function (rpta) {
//            console.log(rpta);
        }
    });
}