$(document).ready(function () {
    $('#example').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla.",
            "info": "Del _START_ al _END_ de _TOTAL_ ",
            "infoEmpty": "Mostrando 0 registros de un total de 0.",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "infoPostFix": "registros",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar ",
//            "searchPlaceholder": "Dato para buscar",
            "zeroRecords": "No se han encontrado coincidencias.",
            "paginate": {
                "first": "Primera",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        }
//        ,
//        "aoColumnDefs": [
//            {"bSortable": false, "aTargets": [2, 3, 4, 5, 6]}
//        ]
    });


});
function ObtenerDatosUsuario(value) {
//    debugger;
    var id = $(value).parents("tr").find("td").eq(0).html();
    var codigo = $(value).parents("tr").find("td").eq(1).html();
    var nombres = $(value).parents("tr").find("td").eq(2).html();
    var apellidos = $(value).parents("tr").find("td").eq(3).html();
    var cadena_apellidos = apellidos.split(' ');
    var ape_paterno = cadena_apellidos[0];
    var ape_materno = cadena_apellidos[1];
    var correo = $(value).parents("tr").find("td").eq(4).html();
    var rol = $(value).parents("tr").find("td").eq(5).html();

    console.log(rol);
    $('#adm_id').val(id);
    $('#adm_nombres').val(nombres);
    $('#adm_ape_paterno').val(ape_paterno);
    $('#adm_ape_materno').val(ape_materno);
    $('#adm_correo').val(correo);
//    if (rol !== '')
    $("#adm_rol option:selected").text(rol);
//    else
//        $("#adm_rol option:selected").text('SELECCIONAR');

}
function EditarUsuario() {
    debugger;
    var id = $('#adm_id').val();
    var nombres = $('#adm_nombres').val();
    var cadena_nombres = nombres.split(' ');
    var nombre = cadena_nombres[0];
    var ape_paterno = $('#adm_ape_paterno').val();
    var ape_materno = $('#adm_ape_materno').val();
    var correo = $('#adm_correo').val();
    var nuevo_codigo = nombre + '_' + ape_paterno;
    var id_rol = $('#adm_rol').val();
    console.log(id_rol);
    var data = 'id=' + id + '&nombres=' + nombres + '&ape_paterno=' + ape_paterno + '&ape_materno=' + ape_materno + '&correo=' + correo + '&nuevo_codigo=' + nuevo_codigo + '&id_rol=' + id_rol;
    var ruta = URLs + 'Permiso/EditarAdmUsuario';
    console.log(data);
    Swal.fire({
        title: '<strong>¿Desea actualizar los datos?</strong>',
////            text: 'Al hacer click en "Sí", se cerrará sesión.',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No',
////        cancelButtonClass: 'btn-danger',
        confirmButtonText: 'Sí'
////        confirmButtonClass: 'btn-success',
////        buttonsStyling: false

    }).then((result) => {
        if (result.value) {
            mostrarLoadingConsulta();
            $.ajax({
                type: 'GET',
                url: ruta,
                data: data,
                contentType: false,
                processData: false,
                success: function (rpta) {
                    ocultarLoadingConsulta();
                    console.log('ok');
                    $('#modal_editar_usuario').modal('hide');
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            console.log('cancelado ...');
        }
    });
}

function mostrarLoadingConsulta() {
    Swal.fire({
        text: 'procesando ...',
        closeOnEsc: false,
        closeOnClickOutside: false,
        button: false,
        showConfirmButton: false
    });
}

function ocultarLoadingConsulta() {
    Swal.close();
}

function convertirMayuscula(l) {
    l.value = l.value.toUpperCase();
}