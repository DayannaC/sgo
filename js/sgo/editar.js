$(function () {
    // $('.timepicker').timepicker({
    //     defaultTime: 'now',
    //     twelveHour: false
    // });
    // $(".timepicker").inputmask();
    $(".fila12").inputmask();
    $('#span-123').inputmask();
    varConnectionCheck = true;
    $.each($("input"), function(index, val) {
        $(this).removeAttr('disabled');
    });
});

$.fn.blockInputs = function () {
    var input = this,
        oldValue,
        oldCursor,
        regex = new RegExp(/^\d{0,4}$/g);
        function mask(value) {
            var output = [];
            for(var i = 0; i < value.length; i++) {
                if(i !== 0 && i % 2 === 0) {
                    output.push(":"); // add the separator
                }
                output.push(value[i]);
            }
            return output.join("");
        }
        function unmask(value) {
            var output = value.replace(new RegExp(/[^\d]/, 'g'), ''); // Remove every non-digit character
            return output;
        }
        function checkSeparator(position, interval) {
          return Math.floor(position / (interval + 1));
        }
        input.on('keydown', function (e){
            var el = e.target;
            oldValue = el.textContent.trim();
            var sel = window.getSelection && window.getSelection();
            var range = sel.getRangeAt(0);
            oldCursor = range.startOffset;
            // oldCursor = el.selectionEnd;
        })
        .on('input', function(e) {
            var el = e.target,
                newCursorPosition,
                newValue = unmask(el.textContent.trim());

            if(newValue.match(regex)) {
                newValue = mask(newValue);
            
                newCursorPosition = oldCursor - checkSeparator(oldCursor, 2) + checkSeparator(oldCursor + (newValue.length - oldValue.length), 2) + (unmask(newValue).length - unmask(oldValue).length);
                if(newValue !== "") {
                    el.textContent = newValue;
                } else {
                    el.textContent = "";
                }
            } else {
                el.textContent = oldValue;
                newCursorPosition = oldCursor;
            }
            var setpos = document.createRange(); 
            var set = window.getSelection(); 
            if (el.childNodes[0]) {
                setpos.setStart(el.childNodes[0], newCursorPosition); 
                setpos.collapse(true); 
                set.removeAllRanges(); 
                // Add range with respect to range object. 
                set.addRange(setpos); 
            }
            // tag.focus(); 
            // el.setSelectionRange(newCursorPosition, newCursorPosition);
        });
}
$('.hdp').blockInputs();

$('.timepicker').blockInputs();

function editarTNavegable(valor, nom_columna) {
    var div_valor = valor.innerHTML;
    var div_id = $(valor).attr('id');

    console.log(valor);

    console.log(div_valor);
    console.log(div_id);
    var data = 'div_valor=' + div_valor + '&div_id=' + div_id + '&nom_columna=' + nom_columna;
    var ruta = URLs + 'sgo/EditarTNavegable';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            console.log('ok');
        }
    });
}

function editarTFuel(valor) {
    var time_td = valor.previousElementSibling;
    var lbs_td = time_td.previousElementSibling;
    var fuel_td = lbs_td.previousElementSibling;
    var tnavegable_id_td = fuel_td.previousElementSibling;
    var actuals = valor.innerHTML;
    var fuel = fuel_td.innerHTML;
    var tnavegable_id = tnavegable_id_td.innerHTML;

    var data = 'fuel=' + fuel + '&tnavegable_id=' + tnavegable_id + '&actuals=' + actuals;
    var ruta = URLs + 'sgo/EditarTFuel';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            console.log('ok');
        }
    });
}

function editarTRutas(valor) {
    console.log(valor);
    var fila_id = $(valor).attr("id");
    var fila = $(valor).attr("class");
    var fila_valor = valor.innerHTML;
    // var row = $(valor).closest("tr");
    if (fila_id == 'remarks') {
        var fila12 = valor.previousElementSibling;
        var fila11 = fila12.previousElementSibling;
        var fila10 = fila11.previousElementSibling;
        var fila9 = fila10.previousElementSibling;
        var fila8 = fila9.previousElementSibling;
        var fila7 = fila8.previousElementSibling;
        var fila6 = fila7.previousElementSibling;
    } else if ((fila_id == 'ata')) {
        var fila11 = valor.previousElementSibling;
        var fila10 = fila11.previousElementSibling;
        var fila9 = fila10.previousElementSibling;
        var fila8 = fila9.previousElementSibling;
        var fila7 = fila8.previousElementSibling;
        var fila6 = fila7.previousElementSibling;
    } else if ((fila_id == 'ate')) {
        var fila10 = valor.previousElementSibling;
        var fila9 = fila10.previousElementSibling;
        var fila8 = fila9.previousElementSibling;
        var fila7 = fila8.previousElementSibling;
        var fila6 = fila7.previousElementSibling;
    } else if ((fila_id == 'remaining')) {
        var fila9 = valor.previousElementSibling;
        var fila8 = fila9.previousElementSibling;
        var fila7 = fila8.previousElementSibling;
        var fila6 = fila7.previousElementSibling;
    } else if ((fila_id == 'flow')) {
        var fila8 = valor.previousElementSibling;
        var fila7 = fila8.previousElementSibling;
        var fila6 = fila7.previousElementSibling;
    } else if ((fila_id == 'actl')) {
        var fila7 = valor.previousElementSibling;
        var fila6 = fila7.previousElementSibling;
    } else {
        var fila6 = valor.previousElementSibling;
    }
    var fila5 = fila6.previousElementSibling;
    var fila4 = fila5.previousElementSibling;
    var fila3 = fila4.previousElementSibling;
    var fila2 = fila3.previousElementSibling;
    var fila1 = fila2.previousElementSibling;
    var fila1_valor = fila1.innerHTML;
    var tnavegable_id = fila1.previousElementSibling;
    var tnavegable_id_valor = tnavegable_id.innerHTML;

    console.log(fila);

    var data = 'fila_valor=' + fila_valor + '&fila_id=' + fila_id + '&tnavegable_id=' + tnavegable_id_valor + '&fila1=' + encodeURIComponent(fila1_valor) + '&fila=' + fila;
    console.log(data);
    var ruta = URLs + 'sgo/EditarTRutas';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            console.log('ok');
        }
    });
}

function restar_fuel_used(valor_columna5, valor_columna3) {
    var resta;
    if (valor_columna5 != '' && valor_columna3 != '') {
        resta = valor_columna5 - valor_columna3;
    }
    else{
        resta = 0;
    }
    return resta;
}

function restar_horas(valor_columna5, valor_columna3) {
    if (valor_columna5 !== '' && valor_columna3 !== '') {
        var horaFin = moment(valor_columna5, "HH:mm");
        var horaInicio = moment(valor_columna3, "HH:mm");
        var hrs = moment.utc(horaFin.diff(horaInicio)).format("HH");
        var min = moment.utc(horaFin.diff(horaInicio)).format("mm");
        return [hrs, min].join(':');
    } else {
        return '';
    }
}

function sumar_horas(hora, min) {
    if (hora !== '' && hora !== '') {
        var min = moment(min, "H:mm").format("mm");
        var hora = moment(hora, "HH:mm").add(min, 'minutes').format("HH:mm");
        return hora;
    } else {
        return '';
    }
}

function obtenerColumna7(td_clase, tnavegable_id, fila2) {
    // debugger
    var td = $('.' + td_clase + ''); //td-columna6
    var tr = td.parents('tr'); //tr
    var children_td_5 = tr.children().eq(4);//td-colummna 5
    var children_td_2 = tr.children().eq(2); //td-colummna 3
    var value_input_children_td_5 = children_td_5[0].firstElementChild.textContent.trim(); //input-value-columna5
    var value_input_children_td_2 = children_td_2[0].firstElementChild.textContent.trim();//input-value-columna3

    if (td_clase !== 'Fuel') {
        var resta = restar_horas(value_input_children_td_5, value_input_children_td_2);
    } else {
        var resta = restar_fuel_used(value_input_children_td_5, value_input_children_td_2);
    }
    var data = 'tnavegable_id=' + tnavegable_id + '&fila2=' + fila2 + '&nombre_td=' + 'valor3' + '&valor=' + resta;
    var ruta = URLs + 'sgo/EditarTNotes';
    // console.log(data);
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            if (rpta) {
                if (td_clase !== 'Fuel') {
                    $('.' + td_clase).html(resta);
                } else {
                    $('.Fuel').html(resta);
                }
            }
        }
    });
}

$(".timepicker").blur(function () {
    var id_td = $(this).attr("id");
    var valor = this.textContent.trim();
    if ($(this).hasClass('off')) {
        generarHoraRuta(valor,'alt0');
    }
    editarTNotes(id_td, valor);
});

$(".fuel").blur(function () {
    var id_td = $(this).attr("id");
    var valor = this.textContent.trim();
    editarTNotes(id_td, valor)
});

function editarTNotes(id_td, valor) {
    var id_cadena = id_td.split('-');
    var fila2 = id_cadena[0];
    var fila3 = id_cadena[1];
    var nombre_td = id_cadena[2];
    var tnavegable_id = id_cadena[3];
    // console.log(id_td);
    // console.log("valor" + valor);
    // console.log(fila3);
    // console.log(nombre_td);
    // console.log(tnavegable_id);
    var data = 'tnavegable_id=' + tnavegable_id + '&fila2=' + fila2 + '&nombre_td=' + nombre_td + '&valor=' + valor;
    var ruta = URLs + 'sgo/EditarTNotes';
    console.log(data);
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            obtenerColumna7(fila3, tnavegable_id, fila2);
        }
    });
}

function generarHoraRuta(hora,alt) {
    if (hora.length == 5) {
        var tbody = $(".truta tbody tr." + alt);
        $.each(tbody, function(k, val) {
            var fila = this.children[12].firstElementChild.id.split('-');
            if (k == 0) {
                if ($(this).hasClass('alt0')) {
                    this.children[12].firstElementChild.textContent = hora;
                }
                var data = 'fila_valor=' + hora + '&id_trutas=' + fila[1] + '&tnavegable_id=' + fila[0] + '&fila=fila12';
                editarTablaRutas(data);
            }
            else{
                if ($(this).hasClass('alt0')) {
                    var ant = tbody[k-1].children[12].firstElementChild.textContent.trim();
                }
                else{
                    if (k == 1) {
                        var ant = tbody[k-1].children[12].lastElementChild.textContent.trim();
                    }
                    else{
                        var ant = tbody[k-1].children[12].firstElementChild.textContent.trim();
                    }
                }
                var minEstimado = this.children[10].innerHTML.trim().split('<br>')[0];
                var horaSumado = sumar_horas(ant,minEstimado);
                this.children[12].firstElementChild.textContent = horaSumado;
                var horaReal = this.children[12].lastElementChild.textContent.trim();
                var valor = horaReal ? horaSumado + '<br>' + horaReal : horaSumado;
                var data = 'fila_valor=' + valor + '&id_trutas=' + fila[1] + '&tnavegable_id=' + fila[0] + '&fila=fila12';
                editarTablaRutas(data);
            }
        });
    }
}

$(".fila12").blur(function(event) {
    var regla = /(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]/g;
    var regla2 = /(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]/g;
    var regla3 = /(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]/g;
    columna11 = this.parentElement.previousElementSibling;
    debugger
    if (this.textContent.trim().length == 5 && regla.test(this.textContent.trim()) == true) {
        var fila = this.id.split('-'); // valor id columna 12
        var horaEstimado = this.previousElementSibling.textContent.trim();
        var padre = $(this.parentElement.parentElement);
        if (padre.hasClass('alt0')) {
            var minStar = $(".off").html().trim();
            if (regla2.test(minStar)) {
                var horaAte = moment(restar_horas(this.textContent.trim(),minStar), "HH:mm").format("H:mm");
                var horaEte = columna11.innerHTML.trim().split('<br>')[0] + '<br>' + horaAte;
                columna11.innerHTML = horaEte;
                //gurdar columna 12
                var data = 'fila_valor=' + horaEstimado + '<br>' + this.textContent.trim() + '&id_trutas=' + fila[1] + '&tnavegable_id=' + fila[0] + '&fila=fila12';
                editarTablaRutas(data);
                //guardar columna 11
                var columna = $(columna11).attr('name').split('-'); // valor id columna 11
                var data = 'fila_valor=' + horaEte + '&id_trutas=' + columna[1] + '&tnavegable_id=' + columna[0] + '&fila=fila11';
                editarTablaRutas(data);
            }
        }
        else{
            var alt = padre.hasClass('alt1') ? 'alt1' : 'alt2' ;
            if ($(this).hasClass('horaAlt')) {
                generarHoraRuta(this.textContent.trim(),alt);
            }
            else{
                var minStar = $("." + alt +" .horaAlt").html().trim();
                if (regla3.test(minStar)) {
                    var horaAte = moment(restar_horas(this.textContent.trim(),minStar), "HH:mm").format("H:mm");
                    var horaEte = columna11.innerHTML.trim().split('<br>')[0] + '<br>' + horaAte;
                    columna11.innerHTML = horaEte;
                    // debugger
                    //gurdar columna 12
                    var data = 'fila_valor=' + horaEstimado + '<br>' + this.textContent.trim() + '&id_trutas=' + fila[1] + '&tnavegable_id=' + fila[0] + '&fila=fila12';
                    editarTablaRutas(data);
                    //guardar columna 11
                    var columna = $(columna11).attr('name').split('-'); // valor id columna 11
                    var data = 'fila_valor=' + horaEte + '&id_trutas=' + columna[1] + '&tnavegable_id=' + columna[0] + '&fila=fila11';
                    editarTablaRutas(data);
                }
            }
        }
    }
    else{
        this.value = '';
    }
});

$(document).on('keypress', '.fila7span', function(event) {
    if (event.which === 13) {
        return false;
    }
});

$(".fila7").blur(function () {
    var columna7 = this.previousElementSibling.textContent.trim() + '<br>' +$(this).html();//obtener valor de td_rem
    var id_rem = $(this).attr('id');//obtener id de td_rem
    var class_rem = 'fila7';
    var id_rem_cadena = id_rem.split('-');
    var tnavegable_id_trutas = id_rem_cadena[1];
    var id_trutas = id_rem_cadena[2];

    var data = 'fila_valor=' + columna7 + '&id_trutas=' + id_trutas + '&tnavegable_id=' + tnavegable_id_trutas + '&fila=' + class_rem;
    editarTablaRutas(data);

    var td = $('#' + id_rem);
    var tr = td.parents('tr');
    var id_tr = tr.attr('id');

    var children_td_8 = tr.children().eq(8);
    var id_actl = $(children_td_8).attr('id');
    var class_actl = $(children_td_8).attr('class');

    if (!$(this).hasClass('alt')) {
        var total = $('#lbs-Total-tfuel').html();
    }
    else{
        var fila_ultima = $(".ban1").prev().prev().prev();
        var fila_ultima_td = fila_ultima.children().eq(7);
        var total = fila_ultima_td[0].firstElementChild.textContent;
    }
    
    var cadena_valor = columna7.split('<br>');
    var columna7_fila1 = cadena_valor[0];
    var columna7_fila2 = cadena_valor[1];
    var resta_columna7_fila2 = columna7_fila2 ? total - columna7_fila2 : '';
    var resta_columna7 = total - columna7_fila1;
    var resta = resta_columna7 + (resta_columna7_fila2 ? '<br>' + resta_columna7_fila2 : '');
    $('#' + id_actl).html(resta);
    var data = 'fila_valor=' + resta + '&id_trutas=' + id_trutas + '&tnavegable_id=' + tnavegable_id_trutas + '&fila=' + class_actl;
    editarTablaRutas(data);
});

$(".fila13").blur(function(event) {
    var fila = this.id.split('-');
    var data = 'fila_valor=' + this.innerHTML.trim() + '&id_trutas=' + fila[1] + '&tnavegable_id=' + fila[0] + '&fila=fila13';
    editarTablaRutas(data);
});

function editar_tabla_rutas(data) {
    var ruta = URLs + 'sgo/EditarTRutas';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            console.log('ok');
        }
    });
}

function editarTablaRutas(data) {
    var ruta = URLs + 'sgo/EditarTablaRutas';
    $.ajax({
        type: 'GET',
        url: ruta,
        data: data,
        contentType: false,
        processData: false,
        success: function (rpta) {
            console.log('ok');
        }
    });
}

$(".btn-without-connection").click(function(event) {
    if (connectionCheck()) {
        alert('Connection')
    }
    else{
        alert('Noooo');
    }
});

function connectionCheck() {
    $.ajax({
        type: 'GET',
        url: URLs + 'Connection',
        // async: false
        // processData: false,
    })
    .done(function() { varConnectionCheck = true })
    .fail(function() { varConnectionCheck = false })
    return varConnectionCheck;
}
