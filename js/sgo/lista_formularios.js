$(document).ready(function () {
    $('#lista_formularios').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla.",
            "info": "Del _START_ al _END_ de _TOTAL_ ",
            "infoEmpty": "Mostrando 0 registros de un total de 0.",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "infoPostFix": "registros",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar ",
//            "searchPlaceholder": "Dato para buscar",
            "zeroRecords": "No se han encontrado coincidencias.",
            "paginate": {
                "first": "Primera",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "scrollX": true
    });
});

var jsonObject = {
    "tableId": "tabla",
    "addButton": "btnAgregar",
    "deleteButton": {"element": "button", "type": "button", "class": "BtnBorrar", "name": "btnBorrar", "text": "-"},
    "totalCells": 11,
    "cellElement": [
        {"element": "label", "class": "font-weight-bold", "name": "name[]", "text": "02/09/2020"},
        {"element": "label", "class": "font-weight-bold", "name": "lastname[]", "text": "000000"},
        {
            "element": "select",
            "name": "usuarios[]",
            "options": [
                {"element": "option", "value": "1", "text": "DAYANNA_CONDORI"}
            ]
        },
        {"element": "button", "type": "button", "class": "btn btn-primary fas fa-edit", "name": ""},
        {
            "element": "select",
            "name": "usuarios[]",
            "options": [
                {"element": "option", "value": "1", "text": "DAYANNA_CONDORI"}
            ]
        },
        {"element": "button", "type": "button", "class": "btn btn-primary fas fa-edit", "name": ""},
        {
            "element": "select",
            "name": "usuarios[]",
            "options": [
                {"element": "option", "value": "1", "text": "DAYANNA_CONDORI"}
            ]
        },
        {"element": "button", "type": "button", "class": "btn btn-primary fas fa-edit", "name": ""},
        {
            "element": "select",
            "name": "usuarios[]",
            "options": [
                {"element": "option", "value": "1", "text": "DAYANNA_CONDORI"}
            ]
        },
        {"element": "button", "type": "button", "class": "btn btn-primary fas fa-edit", "name": ""},
    ]
}
        