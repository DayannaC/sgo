$(document).on('change', '#file', function (event) {
    var myFile = this.files[0];
    if (myFile) {
        if (myFile.type == 'text/html') {
            Swal.fire({
                title: '<strong>Subir Archivo</strong>',
                text: "¿Desea subir el archivo HTML?",
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'No',
                confirmButtonText: 'Sí'
            }).then((result) => {
                if (result.value) {
                    mostrarLoadingConsulta();
                    var reader = new FileReader();
                    reader.addEventListener('load', function (e) {
                        var wrapper = $(decodeURIComponent(escape(e.target.result)))[13];
//                        $("#salida").html(wrapper);

                        var objectData = {};
                        wrapper = wrapper.firstElementChild.children;
                        var a = wrapper[0].children;
                        objectData.titulo = a[0].children[0].textContent.trim().replace(/—/g, '-');
                        objectData.subtitulo = a[0].children[1].textContent.trim();
                        objectData.textofecha = a[1].textContent.trim();
                        var titulosb = ['ete', 'distance', 'avgwind', 'etd', 'eta', 'avgtas', 'altitude'];
                        var b = wrapper[2].firstElementChild.firstElementChild.firstElementChild.children;
                        $.each(b, function (index, val) {
                            objectData[titulosb[index]] = this.lastElementChild.textContent.trim();
                        });
                        var wrapper5_class = wrapper[5].classList.value;
                        if (wrapper5_class == 'hr') {
                            objectData.route = wrapper[4].firstElementChild.firstElementChild.firstElementChild.lastElementChild.textContent.trim();
                        } else {
                            objectData.route = wrapper[4].firstElementChild.lastElementChild.textContent.trim();
                        }

                        if (wrapper5_class == 'hr') {
                            var c = wrapper[6].children[0].firstElementChild.firstElementChild.lastElementChild.children;
                        } else {
                            var c = wrapper[5].children[0].firstElementChild.lastElementChild.children;
                        }

                        var arrayc = [];
                        $.each(c, function (index, val) {
                            var d = {};
                            d.fuel = this.children[0].textContent.trim();
                            d.lbs = this.children[1].textContent.trim();
                            d.time = this.children[2].textContent.trim();
                            arrayc.push(d);
                        });
                        objectData.fuel = arrayc;
                        if (wrapper5_class == 'hr') {
                            var e = wrapper[6].children[1].firstElementChild.lastElementChild.lastElementChild.children;
                        } else {
                            var e = wrapper[5].children[1].firstElementChild.lastElementChild.children;
                        }

                        var arraye = [];
                        $.each(e, function (index, val) {
                            var f = {};
                            f.weight = this.children[0].textContent.trim();
                            f.numero = this.children[1].textContent.trim();
                            arraye.push(f);
                        });
                        objectData.weight = arraye;
                        if (wrapper5_class == 'hr') {
                            var g = wrapper[6].children[2].firstElementChild.lastElementChild.lastElementChild.children;
                        } else {
                            var g = wrapper[5].children[2].firstElementChild.lastElementChild.children;
                        }

                        var arrayg = [];
                        $.each(g, function (index, val) {
                            var h = {};
                            h.info = this.children[0].textContent.trim();
                            h.item = this.children[1].textContent.trim();
                            arrayg.push(h);
                        });
                        objectData.info = arrayg;
                        objectData.ramp = '';
                        if (wrapper5_class == 'hr') {
                            var i = wrapper[7].lastElementChild.firstElementChild.firstElementChild.firstElementChild.lastElementChild.children;
                        } else {
                            var i = wrapper[6].lastElementChild.firstElementChild.lastElementChild.children;
                        }

                        var arrayi = [];
                        $.each(i, function (index, val) {
                            var j = {};
                            j.fila1 = this.children[0].textContent.trim();
                            j.fila2 = this.children[1].textContent.trim();
                            j.fila3 = this.children[2].textContent.trim();
                            arrayi.push(j);
                        });
                        objectData.notes = arrayi;
                        objectData.departureatis = '';
                        objectData.clearance = '';
                        if (wrapper5_class == 'hr') {
                            var k = wrapper[10].firstElementChild.lastElementChild.children;
                        } else {
                            var k = wrapper[9].lastElementChild.children;
                        }

                        var arrayk = [];
                        $.each(k, function (index, val) {
                            var l = {};
                            if ($(this).hasClass('sub-header')) {
                                l.columna = 0;
                                l.fila1 = this.firstElementChild.textContent.trim();
                            } else {
                                l.columna = 1;
                                l.fila1 = this.children[0].innerHTML.trim();
                                l.fila2 = this.children[1].innerHTML.trim();
                                l.fila3 = this.children[2].innerHTML.trim();
                                l.fila4 = this.children[3].innerHTML.trim();
                                l.fila5 = this.children[4].innerHTML.trim();
                                l.fila6 = this.children[5].innerHTML.trim();
                                l.fila7 = this.children[6].innerHTML.trim();
                                l.fila8 = this.children[7].innerHTML.trim();
                                l.fila9 = this.children[8].innerHTML.trim();
                                l.fila10 = this.children[9].innerHTML.trim();
                                l.fila11 = this.children[10].innerHTML.trim();
                            }
                            arrayk.push(l);
                        });
                        objectData.rutas = arrayk;
                        objectData.arrivalatis = '';
                        if (wrapper5_class == 'hr') {
                            var m = wrapper[12].lastElementChild.children;
                        } else {
                            var m = wrapper[11].lastElementChild.children;
                        }
                        var arraym = [];
                        $.each(m, function (index, val) {
                            var n = {};
                            n.fila1 = this.children[0].textContent.trim();
                            n.fila2 = this.children[1].textContent.trim();
                            n.fila3 = this.children[2].textContent.trim();
                            n.fila4 = this.children[3].textContent.trim();
                            n.fila5 = this.children[4].textContent.trim();
                            n.fila6 = this.children[5].textContent.trim();
                            n.fila7 = this.children[6].textContent.trim();
                            n.fila8 = this.children[7].textContent.trim();
                            arraym.push(n);
                        });
                        objectData.latitudes = arraym;
//                if (wrapper5_class == 'hr') {
//                    var o = wrapper[13].children[0].firstElementChild.lastElementChild.firstElementChild.children;
//                } else {
//                    var o = wrapper[12].firstElementChild.firstElementChild.lastElementChild.firstElementChild.children;
//                }
//
//                objectData.flightregion = o[0].textContent.trim();
//                objectData.eet = o[1].textContent.trim();
//                objectData.entry = o[2].textContent.trim();
//                objectData.exit = o[3].textContent.trim();
//                objectData.distanceb = o[4].textContent.trim();

                        if (wrapper5_class == 'hr') {
                            var o_1 = wrapper[13].children[0].firstElementChild.lastElementChild.children;
                        } else {
                            var o_1 = wrapper[12].children[0].firstElementChild.lastElementChild.children;
                        }
                        var arrayo_1 = [];
                        $.each(o_1, function (index, val) {
                            var o_1_1 = {};
                            o_1_1.flightregion = this.children[0].textContent.trim();
                            o_1_1.eet = this.children[1].textContent.trim();
                            o_1_1.entry = this.children[2].textContent.trim();
                            o_1_1.exit = this.children[3].textContent.trim();
                            o_1_1.distanceb = this.children[4].textContent.trim();
                            arrayo_1.push(o_1_1);
                        });
                        objectData.flight_1 = arrayo_1;
                        if (wrapper5_class == 'hr') {
                            if (wrapper[13].children[1] !== undefined) {
                                var o_2 = wrapper[13].children[1].firstElementChild.lastElementChild.children;
                                var arrayo_2 = [];
                                $.each(o_2, function (index, val) {
                                    var o_2_2 = {};
                                    o_2_2.flightregion = this.children[0].textContent.trim();
                                    o_2_2.eet = this.children[1].textContent.trim();
                                    o_2_2.entry = this.children[2].textContent.trim();
                                    o_2_2.exit = this.children[3].textContent.trim();
                                    o_2_2.distanceb = this.children[4].textContent.trim();
                                    arrayo_2.push(o_2_2);
                                });
                                objectData.flight_2 = arrayo_2;
                            }
                        } else {
                            if (wrapper[12].children[1] !== undefined) {
                                var o_2 = wrapper[12].children[1].firstElementChild.lastElementChild.children;
                                var arrayo_2 = [];
                                $.each(o_2, function (index, val) {
                                    var o_2_2 = {};
                                    o_2_2.flightregion = this.children[0].textContent.trim();
                                    o_2_2.eet = this.children[1].textContent.trim();
                                    o_2_2.entry = this.children[2].textContent.trim();
                                    o_2_2.exit = this.children[3].textContent.trim();
                                    o_2_2.distanceb = this.children[4].textContent.trim();
                                    arrayo_2.push(o_2_2);
                                });
                                objectData.flight_2 = arrayo_2;
                            }
                        }

                        if (wrapper5_class == 'hr') {
                            var p = wrapper[14].lastElementChild.children;
                        } else {
                            var p = wrapper[13].lastElementChild.children;
                        }

                        var arrayp = [];
                        $.each(p, function (index, val) {
                            var q = {};
                            q.fila1 = this.children[0].textContent.trim();
                            q.fila2 = this.children[1].textContent.trim();
                            q.fila3 = this.children[2].textContent.trim();
                            q.fila4 = this.children[3].textContent.trim();
                            q.fila5 = this.children[4].textContent.trim();
                            q.fila6 = this.children[5].textContent.trim();
                            q.fila7 = this.children[6].textContent.trim();
                            q.fila8 = this.children[7].textContent.trim();
                            q.fila9 = this.children[8].textContent.trim();
                            q.fila10 = this.children[9].textContent.trim();
                            arrayp.push(q);
                        });
                        objectData.airports = arrayp;
                        debugger
//                        $("#json").html(syntaxHighlight(JSON.stringify(objectData,undefined, 4)));
                        var route = location.origin + '/sgo/enviar'
                        sendRequest(route, objectData, 'POST', function (data, txtStatus) {
                            $("#salida").html('Datos Insetados');
                            $("#json").html('');
                            $("#file").val('');
                            $("#tbody tbody").html(data.responseText);
                            ocultarLoadingConsulta();
                        });
                    });
                    reader.readAsBinaryString(myFile);
                } else if (result.dismiss === Swal.DismissReason.cancel) {
                    console.log('cancelado ...');
                }
            });
        } else {
            $("#file").val('');
            Swal.fire({
                text: "No es un archivo HTML",
                type: 'warning'
            });
        }
    }
});
$(document).ready(function () {
    $('#tbody').DataTable({
        "language": {
            "emptyTable": "No hay datos disponibles en la tabla.",
            "info": "Del _START_ al _END_ de _TOTAL_ ",
            "infoEmpty": "Mostrando 0 registros de un total de 0.",
            "infoFiltered": "(filtrados de un total de _MAX_ registros)",
            "infoPostFix": "registros",
            "lengthMenu": "Mostrar _MENU_ registros",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar ",
//            "searchPlaceholder": "Dato para buscar",
            "zeroRecords": "No se han encontrado coincidencias.",
            "paginate": {
                "first": "Primera",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
//        "aoColumnDefs": [{
//                "aTargets": [, ],
//                "defaultContent": "",
//            }]
//        ,
//        "aoColumnDefs": [
//            {"bSortable": false, "aTargets": [2, 3, 4, 5, 6]}
//        ]
    });
//    $('.eliminar').on('click', function () {
//        debugger;
//        console.log("eliminar");
//        var id = $(this).parents("tr").find("td").eq(0).html();
//        console.log(id);
//        var dato = 'id=' + id;
//        var ruta = URLs + 'SGO/EliminarRegistro';
//        $.ajax({
//            type: "GET",
//            url: ruta,
//            data: data,
//            contentType: false,
//            processData: false,
//            success: function (rpta) {
//                console.log(rpta);
//            }
//        }
//        );
//        var route = location.origin + '/SGO/EliminarRegistro';

//        sendRequest(route, dato, 'GET', function (data, txtStatus) {
//            console.log('OK');
//
//        });

});
function elminarRegistro(id) {
//    console.log(id);
    var dato = 'id=' + id;
    Swal.fire({
        title: '<strong>Eliminar Registro</strong>',
        text: "¿Desea eliminar el registro?",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No',
//        cancelButtonClass: 'btn-danger',
        confirmButtonText: 'Sí'
//        confirmButtonClass: 'btn-success',
//        buttonsStyling: false
    }).then((result) => {
        if (result.value) {
            mostrarLoadingConsulta();
            $.ajax({
                url: URLs + "/SGO/EliminarRegistro",
                type: "GET",
                data: dato,
                contentType: false,
                processData: false,
                success: function () {
                    ocultarLoadingConsulta();
                    MensajeSweetAlert("success", 'Se elimino el registro');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ocultarLoadingConsulta();
                    MensajeSweetAlert("error", 'No hubo conexion, o hay una falla en la sentencia SQL!');
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            console.log('cancelado ...');
        }
    });
}

function aceptar_registro(id) {
    $.ajax({
        url: URLs + "/SGO/BuscarFirma",
        contentType: false,
        processData: false,
        success: function (rpta) {
            console.log('rpta' + rpta);
            if (rpta !== '') {
                Swal.fire({
                    title: '<strong>Aceptar Registro</strong>',
                    text: "¿Desea aceptar el registro?",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'No',
                    confirmButtonText: 'Sí'
                }).then((result) => {
                    if (result.value) {
                        mostrarLoadingConsulta();
                        var dato = 'id=' + id;
                        $.ajax({
                            url: URLs + "/SGO/AceptarRegistroTNavegable",
                            type: "GET",
                            data: dato,
                            contentType: false,
                            processData: false,
                            success: function () {
                                ocultarLoadingConsulta();
                                MensajeSweetAlert("success", 'Se acepto el registro');
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                ocultarLoadingConsulta();
                                MensajeSweetAlert("error", 'No hubo conexion, o hay una falla en la sentencia SQL!');
                            }
                        });
                    } else if (result.dismiss === Swal.DismissReason.cancel) {
                        console.log('cancelado ...');
                    }
                });
            } else {
                Swal.fire({
                    icon: 'error',
                    text: "No se encontro la firma",
                    type: 'warning',
                    showCancelButton: false,
                    confirmButtonText: 'Ok'
                });
            }
        }
    });
}

function cerrar_registro(id) {
    Swal.fire({
        title: '<strong>Cerrar Registro</strong>',
        text: "¿Desea cerrar el registro?",
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No',
        confirmButtonText: 'Sí'
    }).then((result) => {
        if (result.value) {
            mostrarLoadingConsulta();
            var dato = 'id=' + id;
            $.ajax({
                url: URLs + "/SGO/CerrarRegistroTNavegable",
                type: "GET",
                data: dato,
                contentType: false,
                processData: false,
                success: function (data) {
                    ocultarLoadingConsulta();
                    if (data == 'correcto') {
                        MensajeSweetAlert("success", "success", 'Se cerro el registro');
                    } else if (data == 'falta_departureatis') {
                        MensajeSweetAlert("error", "error", 'LLenar departureatis');
                    } else if (data == 'falta_clearance') {
                        MensajeSweetAlert("error", "error", 'LLenar clearance');
                    } else {
                        MensajeSweetAlert("error", "error", 'LLenar ramp');
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    ocultarLoadingConsulta();
                    MensajeSweetAlert("error", 'No hubo conexion, o hay una falla en la sentencia SQL!');
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            console.log('cancelado ...');
        }
    });
}

function MensajeSweetAlert(icon, tipo, mensaje) {
    var color;
    if (tipo == "success") {
        color = "rgba(165, 220, 134, 0.45)";
    } else if (tipo == "warning") {
        color = "rgba(255, 193, 7, 0.54)";
    } else if (tipo == "error") {
        color = "rgba(242, 116, 116, 0.45)";
    }
    Swal.fire({
        icon: icon,
        title: mensaje,
        type: tipo,
        button: "Cerrar",
        timer: "4000",
        backdrop: color
    });
}

function mostrarLoadingConsulta() {
    Swal.fire({
        text: 'procesando ...',
        closeOnEsc: false,
        closeOnClickOutside: false,
        button: false,
    });
}

function ocultarLoadingConsulta() {
    Swal.close();
}