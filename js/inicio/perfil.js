$("#editar_datos").on("click", function () {
    var form_datos = $('#form_editar_datos').serialize();
    var ruta = URLs + 'inicio/EditarDatosUsuario';

    Swal.fire({
        title: '<strong>¿Desea actualizar los datos?</strong>',
        text: 'Al hacer click en "Sí", se cerrará sesión.',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'No',
//        cancelButtonClass: 'btn-danger',
        confirmButtonText: 'Sí'
//        confirmButtonClass: 'btn-success',
//        buttonsStyling: false

    }).then((result) => {
        if (result.value) {
            mostrarLoadingConsulta();
            $.ajax({
                type: 'GET',
                url: ruta,
                data: form_datos,
                contentType: false,
                processData: false,
                success: function (rpta) {
                    ocultarLoadingConsulta();
                    console.log('ok');
                    $('#modal_editar_datos').modal('hide');
                }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
            console.log('cancelado ...');
        }
    });
    console.log(form_datos);
    console.log('editar_datos');
});
$("#cambiar_clave").on("click", function () {
    console.log('cambiar_clave');
});
$("#firmar").on("click", function () {
    console.log('firmar');
});
$("#subir_firma").on("click", function () {
    var ruta = URLs + 'inicio/SubirFirmaUsuario';
    var formData = new FormData();
    var files = $('#image')[0].files[0];
    formData.append('file', files);
    $.ajax({
        url: ruta,
        type: 'post',
        data: formData,
        contentType: false,
        processData: false,
        success: function (response) {
            if (response != 0) {
                console.log('ok');
                console.log(response);
            } else {
                alert('Formato de imagen incorrecto.');
            }
        }
    });
    return false;
});


function mostrarLoadingConsulta() {
    Swal.fire({
        text: 'procesando ...',
        closeOnEsc: false,
        closeOnClickOutside: false,
        button: false
    });
}

function ocultarLoadingConsulta() {
    Swal.close();
}